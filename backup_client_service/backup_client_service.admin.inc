<?php
/**
 * @file
 * backup_client_service.admin.inc Admin functions for Backup Client Service
 * module.
 *
 * @see backup_client_service.module
 *
 * @ingroup backup
 */

/** \addtogroup backup Backup Client */
/* @{ */

/**
 * Returns form for web service options
 */
function backup_client_service_settings_form() {
  module_load_include('inc', 'backup_client', 'backup_client.helper');

  backup_client_check_advanced_help('backup_client_service', 'settings');

  $form['backup_client_service_allow_external_access'] = array(
    '#type' => 'checkbox',
    '#title' => t('Allow external servers access to Backup Client.'),
    '#default_value' => variable_get('backup_client_service_allow_external_access', 0),
    '#description' => t('This option will allow XML-RPC requests from external servers (for example: a Drupal site with the Backup Server module installed). Access will only be granted to servers who provide a username and password that matches an account on this site AND the user on this site has been granted the &quot;access backup client services&quot; privilege.'),
    );

  // Adds collapsable UI to checkboxes.
  drupal_add_js('
var map = {
  "#edit-backup-client-service-allow-external-access" : "#backup-service-option-wrapper",
};

$.each(map, function(checkbox, target) {
  // Initialize
  $(checkbox).is(":checked") ? $(target).show() : $(target).hide();
  // Listener
  $(checkbox).change(function() {  $(target).toggle("slow"); } );
});', 'inline', 'footer');


  $form['backup_client_service_more_options'] = array(
    '#type' => 'fieldset',
    '#title' => t('Sharing and backup options'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#prefix' => '<div id="backup-service-option-wrapper">',
  );

  $form['backup_client_service_more_options']['backup_client_service_allow_info_sharing'] = array(
    '#type' => 'checkbox',
    '#title' => t('Allow external server to retrieve backup information'),
    '#default_value' => variable_get('backup_client_service_allow_info_sharing', 0),
    '#description' => t('Shared information includes: <ul><li>Drupal status</li><li>Apache and PHP settings</li><li>Backup Client settings</li><liNames and paths to backed-up files</li><li>Disk space and memory usage</li><li>Drupal module list and status</li><li>MySQL status</li><li>php settings</li></ul>'),
  );

  $form['backup_client_service_more_options']['backup_client_service_allow_external_backup'] = array(
    '#type' => 'checkbox',
    '#title' => t('Allow external server to trigger a backup'),
    '#default_value' => variable_get('backup_client_service_allow_external_backup', 0),
    '#description' => t('This is required for the Backup Server module to request a backup be made.'),
    '#suffix' => '</div>',
  );

  return system_settings_form($form);
}

/* }@ */

