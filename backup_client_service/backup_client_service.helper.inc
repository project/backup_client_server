<?php
/**
 * @file
 * backup_client_service.helper.inc Helper functions for Backup Client Service
 * module.
 *
 * @see backup_client_service.module
 *
 * @ingroup backup
 */

/** \addtogroup backup Backup Client */
/* @{ */

/**
 * Return access control results for provided user information.
 */
function backup_client_service_check_user($user, $password) {
  $uid = db_result(db_query("SELECT uid FROM {users} WHERE name = '%s' AND pass = '%s'", $user, $password));
  $account = user_load(array('uid' => $uid));

  return user_access('access backup client services', $account);
}

/**
 * Returns information about backups and website parameters.
 *
 * @param $user
 *   Username provided by web service call.
 * @param $password
 *   Password provided by web service call.
 * @param $date
 *   Date to limit returned backup results by.
 * @returns
 *   Returns array of arrays of information.
 */
function backup_client_service_info($user, $password, $date) {
  global $db_url;

  if (!backup_client_service_check_user($user, $password)) {
    return FALSE;
  }

  // Load .install files
  include_once './includes/install.inc';
  drupal_load_updates();
  module_load_include('inc', 'update', 'update.fetch');
  module_load_include('inc', 'update', 'update.compare');

  _update_refresh();

  // Get run-time requirements and status information.
  $requirements = module_invoke_all('requirements', 'runtime');
  usort($requirements, '_system_sort_requirements');
  $output['requirements'] = $requirements;

  // Add node information;
  $output['counts'] = backup_client_service_node_stats();

  // This will be false if no connection can be made.
  if ($available = update_get_available(TRUE)) {
    // Get module list by project. This captures contrib modules 
    // that do not have entries or projects with Drupal.org
    $result = db_query("SELECT name, info FROM {system} WHERE type = 'module' AND status = 1");
    while ($module = db_fetch_object($result)) {
      $info = unserialize($module->info);
      if (isset($info['project'])) {
        $modules[$info['project']] = $info['name'];
      }
      else {
        $modules[$module->name] = $info['name'];
      }
    }
    
    // Get info from Drupal.org
    $module_info = update_calculate_project_data($available);

    // Merge update status info with contrib list.
    $modules = array_merge($modules, $module_info);
  
    // Add info for modules with no info (ie. contrib modules with no entries).
    foreach ($modules as $key => $module) {
      if (!is_array($module)) {
        unset($modules[$key]);
        $modules[$key]['title'] = $module;
        $modules[$key]['existing_version'] = t('N/A');
        $modules[$key]['recommended'] = t('N/A');
        $modules[$key]['status'] = -2;
        $modules[$key]['releases']= array(t('N/A') => array('release_link' => ''));
        $modules[$key]['datestamp'] = time();
      }
    }
  
    // Allow other modules to alter list (ie. opt out/in for being monitored).
    drupal_alter('backup_client_service_modules', $modules);
  
    $output['modules'] = $modules;
  }

  // Get MySQL info.
  $result = db_query('SHOW STATUS');
  while ($status = db_fetch_array($result)) {
    $mysql_stats[$status['Variable_name']] = $status['Value'];
  }

  // Add MySQL data and filter out zero values.
  $output['mysql_stat'] = array_filter($mysql_stats);

  // Get server settings
  $output['server'] = array_merge($_SERVER, $_ENV);
  $output['server']['disk_free_space'] = disk_free_space('.');
  $output['server']['php_version'] = phpversion();
  $output['server']['php_loaded_extensions'] = get_loaded_extensions();
  $output['server']['php_max_execution_time'] = ini_get('max_execution_time');
  $output['server']['php_memory_limit'] = ini_get('memory_limit');
  $output['server']['php_memory_usage'] = memory_get_usage();
  
  // Add in Drupal performance settings.
  $output['server']['Drupal caching mode'] = variable_get('cache', CACHE_DISABLED);
  $output['server']['Drupal CSS compression'] = variable_get('preprocess_css', 0);
  $output['server']['Drupal JS compression'] = variable_get('preprocess_js', 0);
  $output['server']['Drupal cache lifetime'] = variable_get('cache_lifetime', 0);

  // Get all Backup client settings.
  $result = db_query("SELECT * FROM {variable} WHERE name REGEXP 'backup_client' ORDER BY name");

  // Blacklist of settings to exclude.
  $blacklist = array();

  // Allow other modules to alter blacklist.
  drupal_alter('backup_client_service_blacklist', $blacklist);

  // Hard code S3 keys.
  $blacklist = array_merge($blacklist, array(
    'backup_client_s3_secret_access_key',
    'backup_client_s3_access_key',
  ));

  while ($value = db_fetch_object($result)) {
    // Check for blacklisted items.
    if (in_array($value->name, $blacklist)) {
      if (unserialize($value->value) != '') {
        $value->value = serialize(t('Value set (not shown)'));
      }
      else {
        $value->value = serialize(t('Value not set (not shown)'));
      }
    }
    $output['settings'][$value->name] = unserialize($value->value);
  }

  if (module_exists('backup_client')) {

    // Update files before adding to output.
    module_load_include('inc', 'backup_client', 'backup_client.helper');
    backup_client_sync_database_to_backup_directory();

    if (module_exists('backup_client_s3')) {
      // Update files before adding to output.
      module_load_include('inc', 'backup_client_s3', 'backup_client_s3.helper');
      backup_client_s3_sync_files();
    }

    // Get all data from backup_client table
    $results = db_query('SELECT * FROM {backup_client} WHERE started > %d AND status = 1', $date);

    $output['backups'] = array();
    while ($result = db_fetch_array($results)) {
      if ($result['type'] == 's3_backup') {
        /** @todo: update S3 link */
      }
      $output['backups'][] = $result;
    }
  }
  else {
      $output['backups'] = array();
  }

  return $output;
}

function backup_client_service_node_stats() {

  $oldest = db_fetch_object(db_query('SELECT MIN(changed) AS changed, MIN(created) AS created FROM {node}'));

  // Setup variables.
  $types = array('created', 'changed');
  $week = 60*60*24*7;
  $now = time();

  // Cycle through two node date types.
  if($oldest->created > 0) {
    foreach ($types as $type) {
      $max_time = $now;
      while ($max_time > $oldest->created) {
        $min_time = $max_time - $week;
        $query = "SELECT COUNT(nid) AS count FROM {node} WHERE ". $type ." <= %d AND ". $type ." > %d";
  
        $output['nodes-'. $type][$max_time] = db_result(db_query($query, $max_time, $min_time));
        $max_time = $min_time;
      }
    }
  }

  $oldest_user = db_result(db_query('SELECT MIN(created) AS created FROM {users} WHERE created <> 0'));

  // Build array of user data.
  $max_time = $now;
  while ($max_time > $oldest_user) {
    $min_time = $max_time - $week;
    $query = "SELECT COUNT(uid) AS count FROM {users} WHERE created <= %d AND created > %d";

    $output['users-created'][$max_time] = db_result(db_query($query, $max_time, $min_time));
    $max_time = $min_time;
  }

  return $output;
}

/**
 * Triggers backup of website for web service call.
 *
 * @param $user
 *   Username provided by web service call.
 * @param $password
 *   Password provided by web service call.
 * @returns
 *   Returns website backup information.
 */
function backup_client_service_backup_website($user, $password) {

  if (!backup_client_service_check_user($user, $password)) {
    return FALSE;
  }

  module_load_include('inc', 'backup_client', 'backup_client.helper');

  // Backup website
  $bid = backup_client_make_tar_backup(
    array(
      'note' => t('Service triggered dump by: @user', array('@user' => $user)),
      'gzip_tar' => variable_get('backup_client_gzip_tar_file', 1),
      'add_info' => variable_get('backup_client_add_info', 0),
      'only_recent_sql' => variable_get('backup_client_only_recent_mysql', 0),
      'verbose' => FALSE,
    )
  );

  if ($bid) {
    // Get all data from backup_client table
    $results = db_query('SELECT * FROM {backup_client} WHERE bid = %d', $bid);

    $output['backups'] = array();
    while ($result = db_fetch_array($results)) {
      $output['backups'][] = $result;
    }

    return $output;
  }
}

/**
 * Triggers backup of database for web service call.
 *
 * @param $user
 *   Username provided by web service call.
 * @param $password
 *   Password provided by web service call.
 * @returns
 *   Returns database backup information.
 */
function backup_client_service_backup_database($user, $password) {
  global $db_url;

  if (!backup_client_service_check_user($user, $password)) {
    return FALSE;
  }

  module_load_include('inc', 'backup_client', 'backup_client.helper');

  // Normalize $db_url to an array and check for multiple database dump
  if (!is_array($db_url) || !variable_get('backup_client_dump_all_databases', TRUE)) {
    $databases = array('default');
  }
  else {
    $databases = array_keys($db_url);
  }

  $success = TRUE;
  // Dump database before tar'ing site.
  // Loop through databases
  $bid = backup_client_next_bid();
  foreach ($databases as $database) {
    $result = backup_client_make_mysqldump(
      array(
        'bid' => $bid,
        'note' => t('Service triggered dump by: @origin', array('@origin' => $_SERVER['HTTP_ORIGIN'])),
        'database' => $database,
      )
    );

    $success = $success && $result;
  }

  if ($success) {
    // Get all data from backup_client table
    $results = db_query('SELECT * FROM {backup_client} WHERE bid = %d', $bid);

    $output['backups'] = array();
    while ($result = db_fetch_array($results)) {
      $output['backups'][] = $result;
    }

    return $output;
  }
}

/**
 * Removes extra backups for web service call.
 *
 * @param $user
 *   Username provided by web service call.
 * @param $password
 *   Password provided by web service call.
 * @returns
 *   Returns array of file deletion counts.
 */
function backup_client_service_backup_delete($user, $password) {

  if (!backup_client_service_check_user($user, $password)) {
    return FALSE;
  }

  module_load_include('inc', 'backup_client', 'backup_client.helper');

  $output['websites_deleted'] = backup_client_remove_extra_files('tar_backup', variable_get('backup_client_number_of_backups_to_keep', '100'));

  $output['databases_deleted'] = backup_client_remove_extra_files('mysqldump', variable_get('backup_client_number_of_mysqldumps_to_keep', '100'));

  return $output;
}

/* }@ */

