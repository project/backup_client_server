<?php
/**
 * @file
 * backup_client_service.module Backup Client Service module provides
 * web services to support the Backup Service module.
 *
 * @see backup_client.module
 * @see backup_service.module
 *
 * @ingroup backup
 */

/** \addtogroup backup Backup Client */
/* @{ */

/**
 * Implementation of hook_perm().
 */
function backup_client_service_perm() {
  return array('access backup client services');
}

/**
 * Implementation of hook_menu().
 */
function backup_client_service_menu() {

  $items['admin/build/backupclient/configuration/client'] = array(
    'title' => 'Client <-> Server',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('backup_client_service_settings_form'),
    'access arguments' => array('administer backup client'),
    'weight' => 5,
    'file' => 'backup_client_service.admin.inc',
    'type' => MENU_LOCAL_TASK,
  );

  return $items;
}

/**
 * Implementation of hook_xlmrpc().
 */
function backup_client_service_xmlrpc() {
  module_load_include('inc', 'backup_client_service', 'backup_client_service.helper');

  $services = array();

  // Return no services if access not allowed.
  if (!variable_get('backup_client_service_allow_external_access', 0)) {
    return $services;
  }

  if (variable_get('backup_client_service_allow_info_sharing', 0)) {
    $services[] = array(
      'backup.info',
      'backup_client_service_info',
      array('struct', 'string', 'string', 'int'),
      t('Returns an array of Backup Client information.'),
    );
  }

  if (variable_get('backup_client_service_allow_external_backup', 0)) {
    $services[] = array(
      'backup.website',
      'backup_client_service_backup_website',
      array('struct', 'string', 'string'),
      t('Returns an array of Backup Client information.'),
    );

    $services[] = array(
      'backup.database',
      'backup_client_service_backup_database',
      array('struct', 'string', 'string'),
      t('Returns an array of Backup Client information.'),
    );
  }

  if (variable_get('backup_client_service_allow_file_removal', 0)) {
    $services[] = array(
      'backup.delete',
      'backup_client_service_backup_delete',
      array('struct', 'string', 'string'),
      t('Returns an array of Backup Client information.'),
    );
  }

  return $services;
}

/* }@ */
