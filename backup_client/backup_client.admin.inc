<?php
/**
 * @file
 *   backup_client.admin.inc Provides admin forms for managing and configuring
 *   backup_client.
 *
 * @see backup_client.module
 *
 * @ingroup backup
 */

/**
 * Builds table of existing backups and appends form for submitting a request
 * to create another backup.
 *
 * @returns
 *  Backup admin page.
 */
function backup_client_tar_admin_page() {

  module_load_include('inc', 'backup_client', 'backup_client.helper');
  backup_client_check_advanced_help('backup_client', 'website');
  backup_client_sync_database_to_backup_directory();
  backup_client_check_for_cron_deletions();

  $pager_limit = variable_get('backup_client_pager_number', '10');

  $header = array(
    array('data' => t('#') , 'field' => 'bid', 'sort' => 'desc'),
    array('data' => t('Filename'), 'field' => 'filename'),
    array('data' => t('Size'), 'field' => 'size'),
    array('data' => t('Seconds'), 'field' => 'duration'),
    array('data' => t('Date'), 'field' => 'started'),
    array('data' => t('Note'), 'field' => 'note'),
  );

  $sql = "SELECT * FROM {backup_client} WHERE type = 'tar_backup' AND status = 1";
  $result = pager_query($sql . tablesort_sql($header) , $pager_limit);
  while ($file = db_fetch_object($result)) {
    $rows[] = array(
      $file->bid,
      '<a href="'. base_path() . check_plain($file->filepath) .'">'. check_plain($file->filename) .'</a>',
      format_size($file->size),
      $file->duration,
      date('Y-m-d h:iA', $file->started),
      check_plain($file->note),
    );
  }

  if ($rows) {
    $pager = theme('pager', NULL, $pager_limit, 0);
    if (!empty($pager)) {
      $rows[] = array(array('data' => $pager, 'colspan' => '5'));
    }
    $output .= theme('table', $header, $rows, array() , '<h3>'. t('Website Backups') .'</h3>');
  }
  else {
    $output .= t('No backups found.');
  }

  if (user_access('create backups')) {
    $output .= drupal_get_form('backup_client_tar_form');
  }

  return $output;
}

/**
 * Defines the backup form.
 */
function backup_client_tar_form() {
  global $db_url;

  $form['backup_client_tar_notes'] = array(
    '#type' => 'textfield',
    '#title' => t('Notes for this website backup'),
  );

  $form['backup_client_more_options'] = array(
    '#type' => 'fieldset',
    '#title' => t('Options (this backup only)'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#description' => t('To change these settings for all future backups goto the') . ' ' . l(t('General Configuration page') , 'admin/build/backupclient/configuration', array('query' => array('open' => 'website'))),
    );

  if (is_array($db_url) && count((array) $db_url) > 1) {
    $form['backup_client_more_options']['backup_client_dump_all_databases'] = array(
    '#type' => 'checkbox',
    '#title' => t('Dump all databases defined in settings.php: %databases', array('%databases' => implode(', ', array_keys($db_url)))),
    '#default_value' => variable_get('backup_client_dump_all_databases', TRUE),
    '#description' => t('More than one database is defined in your settings.php file. Uncheck this option if you only want to dump the default database.'),
    );
  }

  $form['backup_client_more_options']['backup_client_gzip_tar_file'] = array(
  '#type' => 'checkbox',
  '#title' => t('GZIP the output'),
  '#default_value' => variable_get('backup_client_gzip_tar_file', 1) && backup_client_test_for_extension('zlib'),
  '#disabled' => !backup_client_test_for_extension('zlib'),
  '#description' => !backup_client_test_for_extension('zlib') ? t('zilb library unavailable') : t('The resulting website backup will be compressed and have the extension .tar.gz'),
  );

  $form['backup_client_more_options']['backup_client_only_recent_mysql'] = array(
  '#type' => 'checkbox',
  '#title' => t('Include only the most recent database backup(s)'),
  '#default_value' => variable_get('backup_client_only_recent_mysql', 0),
  );

  $form['backup_client_more_options']['backup_client_add_info'] = array(
  '#type' => 'checkbox',
  '#title' => t('Add backup date to site name'),
  '#default_value' => variable_get('backup_client_add_info', 0),
  '#description' => t('The backup date will be appended to the site name in the database backup (ex: !name)', array('!name' => variable_get('site_name', '') .' '. date('Y-m-d-H-i'))),
  );

  // Test for and add PEAR Archive class.
  // From http://pear.php.net/package/Archive_Tar
  if (!backup_client_test_for_include('PEAR.php')) {
    drupal_set_message( t('Cannot locate PEAR.php! Please download from !link and install the file in this module&#146;s folder.', array('!link' => '<a href="http://pear.php.net/package/PEAR">http://pear.php.net/package/PEAR</a>')) , 'error');
    $error = TRUE;
  }

  if (!backup_client_test_for_include('Tar.php')) {
    drupal_set_message( t('Cannot locate Tar.php! Please download from !link and install the file in this module&#146;s folder.', array('!link' => '<a href="http://pear.php.net/package/Archive_Tar">http://pear.php.net/package/Archive_Tar</a>')) , 'error');
    $error = TRUE;
  }

  if (!$error) {
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Backup website'),
      );
  }

  return $form;
}

/**
 * Submit handler for backup form. Calls the tar function with the form's values.
 */
function backup_client_tar_form_submit($form, &$form_state) {

  backup_client_make_tar_backup(
    array(
      'note' => $form_state['values']['backup_client_tar_notes'],
      'gzip' => $form_state['values']['backup_client_gzip_tar_file'],
      'add_info' => $form_state['values']['backup_client_add_info'],
      'only_recent_sql' => $form_state['values']['backup_client_only_recent_mysql'],
      'dump_multiple' => $form_state['values']['backup_client_dump_all_databases']
    )
  );
}

/**
 * Builds table of existing dumps and appends form for submitting a request
 * to create another database dump.
 *
 * @returns
 *  Backup admin page.
 */
function backup_client_mysqldump_admin_page() {

  module_load_include('inc', 'backup_client', 'backup_client.helper');
  backup_client_check_advanced_help('backup_client', 'database');
  backup_client_sync_database_to_backup_directory();
  backup_client_check_for_cron_deletions();

  $pager_limit = variable_get('backup_client_pager_number', '10');

  $header = array(
    array('data' => t('#'), 'field' => 'bid', 'sort' => 'desc'),
    array('data' => t('Filename'), 'field' => 'filename'),
    array('data' => t('Size'), 'field' => 'size'),
    array('data' => t('Seconds'), 'field' => 'duration'),
    array('data' => t('Date'), 'field' => 'started'),
    array('data' => t('Note'), 'field' => 'note'),
  );

  $sql = "SELECT * FROM {backup_client} WHERE type = 'mysqldump' AND status = 1";
  $result = pager_query($sql . tablesort_sql($header) , $pager_limit);
  while ($file = db_fetch_object($result)) {
    $rows[] = array(
      $file->bid,
      '<a href="'. base_path() . check_plain($file->filepath) .'">'. check_plain($file->filename) .'</a>',
      format_size($file->size),
      $file->duration,
      date('Y-m-d h:iA', $file->started),
      check_plain($file->note),
    );
  }

  if ($rows) {
    $pager = theme('pager', NULL, $pager_limit, 0);
    if (!empty($pager)) {
      $rows[] = array(array('data' => $pager, 'colspan' => '5'));
    }
    $output .= theme('table', $header, $rows, array() , '<h3>'. t('Database Backups') .'</h3>');
  }
  else {
    $output .= t('No backups found.');
  }

  if (user_access('create backups')) {
    $output .= drupal_get_form('backup_client_mysqldump_form');
  }

  return $output;
}

/**
 * Defines the database dump form.
 */
function backup_client_mysqldump_form() {
  global $db_url;

  $form['backup_client_notes'] = array(
  '#type' => 'textfield',
  '#title' => t('Notes for this database backup'),
  );

  $form['backup_client_more_options'] = array(
    '#type' => 'fieldset',
    '#title' => t('Options (this backup only)'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#description' => t('To change these settings for all future dumps goto the') . ' ' . l(t('General Configuration page') , 'admin/build/backupclient/configuration', array('query' => array('open' => 'database'))),
  );

  if (is_array($db_url) && count((array) $db_url) > 1) {
    $form['backup_client_more_options']['backup_client_dump_all_databases'] = array(
    '#type' => 'checkbox',
    '#title' => t('Dump all databases defined in settings.php: %databases', array('%databases' => implode(', ', array_keys($db_url)))),
    '#default_value' => variable_get('backup_client_dump_all_databases', TRUE),
    '#description' => t('More than one database is defined in your settings.php file. Uncheck this option if you only want to dump the default database.'),
    );
  }

  $form['backup_client_more_options']['backup_client_gzip_data'] = array(
    '#type' => 'checkbox',
    '#title' => t('GZIP the output'),
    '#disabled' => !backup_client_test_for_extension('zlib'),
    '#default_value' => 1 && backup_client_test_for_extension('zlib'),
    '#description' => !backup_client_test_for_extension('zlib') ? t('zilb library unavailable') : '',
  );

  $form['backup_client_more_options']['backup_client_drop_table_statement'] = array(
    '#type' => 'checkbox',
    '#title' => t('Add DROP TABLE statements'),
    '#default_value' => 1,
  );

  $form['backup_client_more_options']['backup_client_create_table_statement'] = array(
    '#type' => 'checkbox',
    '#title' => t('Add CREATE TABLE statements'),
    '#default_value' => 1,
  );

  $form['backup_client_more_options']['backup_client_add_info'] = array(
    '#type' => 'checkbox',
    '#title' => t('Add backup date to site name.'),
    '#default_value' => variable_get('backup_client_add_info', 0),
    '#description' => t('The backup date will be appended to the site name in the database backup. For example:') .' ('. variable_get('site_name', '') .' '. date('Y-m-d-H-i') . ')',
  );

  if ($mysqldump = backup_client_find_executable('mysqldump')) {
    $form['backup_client_more_options']['backup_client_mysql_options']['system'] = array(
      '#type' => 'fieldset',
      '#title' => t('Shell command (advanced feature)'),
      '#weight' => 2,
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );

    $form['backup_client_more_options']['backup_client_mysql_options']['system']['shell_mysqldump'] = array(
      '#type' => 'submit',
      '#value' => t('Shell command:') .' '. $mysqldump,
      );

    $form['backup_client_more_options']['backup_client_mysql_options']['system']['markup'] = array(
    '#value' => '<p>'. t("Pressing this button will make a system call to mysqldump and write the default database to the server. The options: '%gzip', '%drop', and '%create' are added as arguments.", array(
      '%direct' => 'Direct mysqldump output',
      '%gzip' => 'GZIP the output',
      '%drop' => 'Add DROP TABLE statements',
      '%create' => 'Add CREATE TABLE statements',
      )) .'</p>',
    );
  }

  $form['submit'] = array(
    '#type' => 'submit',
    '#weight' => 3,
    '#value' => t('Backup database'),
    );

  return $form;
}

/**
 * Submit handler for database dump form. Calls the appropriate mysqldump
 * function with the form's values.
 */
function backup_client_mysqldump_form_submit($form, &$form_state) {
  global $db_url;

  $values = $form_state['values'];

  // Fire off shell command if button was pushed
  if (strpos($values['op'], t('Shell command:')) === 0) {
    backup_client_mysqldump_shell_command(
      '',
      $values['backup_client_notes'],
      $values['backup_client_create_table_statement'],
      $values['backup_client_drop_table_statement'],
      $values['backup_client_gzip_data']
      );
    return;
  }

  // Add date information to sitename before sql dump if option is set.
  if ($values['backup_client_add_info']) {
    // Get sitename for changing during mysqldump
    $sitename = variable_get('site_name', 'Drupal');
    variable_set('site_name', "$sitename backup ". $date = date('Y-m-d-H-i'));
  }

  // Normalize $db_url to an array and check for multiple database dump
  if (!is_array($db_url) || !$values['backup_client_dump_all_databases']) {
    $databases = array('default');
  }
  else {
    $databases = array_keys($db_url);
  }

  // Generate new backup ID
  $bid = backup_client_next_bid();

  // Dump database before tar'ing site.
  // Loop through databases
  foreach ($databases as $database) {
    backup_client_make_mysqldump(
      array(
        'bid' => $bid,
        'note' => $values['backup_client_notes'],
        'create_table_structure' => $values['backup_client_create_table_statement'],
        'drop_table_statement' => $values['backup_client_drop_table_statement'],
        'gzip' => $values['backup_client_gzip_data'],
        'database' => $database,
      )
    );
  }

  // Change sitename back after sql dump.
  if (variable_get('backup_client_add_info', 0)) {
    variable_set('site_name', $sitename);
  }
}

/**
 * Returns settings page for general options.
 */
function backup_client_general_settings_page() {
  module_load_include('inc', 'backup_client', 'backup_client.helper');

  // @todo: On some sites this file doesn't get included.
  module_load_include('inc', 'token', 'token.pages');
  backup_client_check_advanced_help('backup_client', 'settings-general');

  $output = drupal_get_form('backup_client_general_settings_form');
  return $output;
}

/**
 * Defines the general settings form.
 */
function backup_client_general_settings_form() {
  global $db_url;

  $form['backup_client_file_options'] = array(
    '#type' => 'fieldset',
    '#title' => t('General options'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['backup_client_file_options']['backup_client_backup_location'] = array(
    '#type' => 'textfield',
    '#title' => t('Location for backup files'),
    '#default_value' => variable_get('backup_client_backup_location', file_directory_path() . DEFAULT_BACKUP_LOCATION),
    '#after_build' => array('backup_client_check_backup_directory'),
  );

  $form['backup_client_file_options']['backup_client_gzip_tar_file'] = array(
    '#type' => 'checkbox',
    '#title' => t('GZIP the output'),
    '#default_value' => variable_get('backup_client_gzip_tar_file', 1) && backup_client_test_for_extension('zlib'),
    '#disabled' => !backup_client_test_for_extension('zlib'),
    '#description' => !backup_client_test_for_extension('zlib') ? t('zilb library unavailable') : t('The resulting website backups and database dumps will be gziped and will have the added extension .gz'),
  );

  $form['backup_client_file_options']['backup_client_chmod_to_600'] = array(
    '#type' => 'checkbox',
    '#title' => t('Change file permission to 600 (-rw-------)'),
    '#default_value' => variable_get('backup_client_chmod_to_600', 1),
    '#description' => t('Website backup and database dump files will have their permissions set to 600 (r+w for user: !user only). This should increase security in shared hosting environments.', array('!user' => '<strong><i>'. $_ENV['USER'] . '</i></strong>')),
  );

  $form['backup_client_file_options']['backup_client_add_info'] = array(
    '#type' => 'checkbox',
    '#title' => t('Add backup date to site name.'),
    '#default_value' => variable_get('backup_client_add_info', 0),
    '#description' => t('The backup date will be appended to the site name in the database backup. For example:') .' ('. variable_get('site_name', '') .' '. date('Y-m-d-H-i') . ')',
  );

  $options = array(
    'off' => t('Turn off advanced help (including install/enable suggestion) !href', array('!href' => l(t('http://drupal.org/project/advanced_help'), 'http://drupal.org/project/advanced_help'))),
    'popup' => t('Open advanced help in new window'),
    'page' => t('Open advanced help in new page'),
  );

  $form['backup_client_file_options']['backup_client_advanced_help_setting'] = array(
    '#type' => 'radios',
    '#title' => t('Advanced help options'),
    '#options' => $options,
    '#default_value' => variable_get('backup_client_advanced_help_setting', 'popup'),
  );

  $form['backup_client_file_options']['backup_client_pager_number'] = array(
    '#type' => 'select',
    '#title' => t('Number of rows in backup tables'),
    '#default_value' => variable_get('backup_client_pager_number', '10'),
    '#options' => array(
      '5' => '5',
      '10' => '10',
      '20' => '20',
      '40' => '40',
      '80' => '80',
      '100' => '200',
      '100000' => t('Infinity'),
    ),
  );

  $form['backup_client_website_options'] = array(
    '#type' => 'fieldset',
    '#title' => t('Website backup options'),
    '#collapsible' => TRUE,
    '#collapsed' => $_GET['open'] != 'website',
  );

  $form['backup_client_website_options']['backup_client_tar_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Token pattern for website backup filenames'),
    '#default_value' => variable_get('backup_client_tar_name', '[backup-site-timestamp]-[random-characters]-[backup-site-url]'),
    '#description' => t('Example: %example', array('%example' => token_replace(variable_get('backup_client_tar_name', '[backup-site-timestamp]-[random-characters]-[backup-site-url]')))),
  );

  backup_client_add_token_forms($form, 'backup_client_website_options');

  $form['backup_client_website_options']['backup_client_only_recent_mysql'] = array(
    '#type' => 'checkbox',
    '#title' => t('Include only the most recent database backup(s) in the website backup.'),
    '#default_value' => variable_get('backup_client_only_recent_mysql', 0),
  );

  $form['backup_client_website_options']['backup_client_excluded_directories'] = array(
    '#type' => 'textarea',
    '#title' => t('Do not include in tar file'),
    '#default_value' => variable_get('backup_client_excluded_directories', ''),
    '#description' => t('Enter one pattern per line as Drupal paths. The \'*\' character is a wildcard. Example: to exclude all the files in the files directory use %files', array('%files' => file_directory_path() .'/*')),
  );

  $form['backup_client_database_options'] = array(
    '#type' => 'fieldset',
    '#title' => t('Database backup options'),
    '#collapsible' => TRUE,
    '#collapsed' => $_GET['open'] != 'database',
  );

  $form['backup_client_database_options']['backup_client_database_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Token pattern for database backup filenames'),
    '#default_value' => variable_get('backup_client_database_name', '[backup-site-timestamp]-[random-characters]-[backup-database-name]'),
    '#description' => t('Example: %example', array('%example' => token_replace(variable_get('backup_client_database_name', '[backup-site-timestamp]-[random-characters]-[backup-database-name]')))) .'<br />'. t('Note: if your site has multiple databases and you are dumping more than one -- you should not remove the [backup-database-name] token'),
  );

  backup_client_add_token_forms($form, 'backup_client_database_options');

  if (is_array($db_url) && count((array) $db_url) > 1) {
    $form['backup_client_database_options']['backup_client_dump_all_databases'] = array(
    '#type' => 'checkbox',
    '#title' => t('Dump all databases defined in settings.php: %databases', array('%databases' => implode(', ', array_keys($db_url)))),
    '#default_value' => variable_get('backup_client_dump_all_databases', TRUE),
    '#description' => t('More than one database is defined in your settings.php file. Uncheck this option if you only want to dump the default database.'),
    );

    backup_client_add_database_forms($form);
  }

  $form['backup_client_database_options']['backup_client_drop_table_statement'] = array(
  '#type' => 'checkbox',
  '#title' => t('Add DROP TABLE statements'),
  '#default_value' => variable_get('backup_client_drop_table_statement', 1),
  );

  $form['backup_client_database_options']['backup_client_create_table_statement'] = array(
  '#type' => 'checkbox',
  '#title' => t('Add CREATE TABLE statements'),
  '#default_value' => variable_get('backup_client_create_table_statement', 1),
  );

  $form['backup_client_database_options']['backup_client_skip_tables'] = array(
  '#type' => 'checkbox',
  '#title' => t('Do not backup data from the <i>accesslog, cache, cache_*, and watchdog</i> tables'),
  '#default_value' => variable_get('backup_client_skip_tables', 0),
  '#description' => t('The resulting backup will contain information for the tables\' structure but not their data. This can help reduce the size of your database backups'),
  );

  $form['backup_client_database_options']['backup_client_table_source'] = array(
  '#type' => 'radios',
  '#title' => t('Tables for database backup are determined by'),
  '#options' => array('mysql' => t('getting table information via mysql query') , 'schema' => t('getting table information via the Drupal schema API')),
  '#default_value' => variable_get('backup_client_table_source', 'mysql'),
  '#description' => t('Choosing the mysql query option will backup all tables present in your active database. Choosing the Drupal schema API will backup those tables that are registered by their their respective Drupal modules.'),
  );

  return system_settings_form($form);
}

/**
 *
 */
function backup_client_add_database_forms(&$form) {
  global $db_url;

  $form['backup_client_database_options']['dbs'] = array(
    '#type' => 'fieldset',
    '#title' => t('Select tables to backup for additional databases.'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $dbs = $db_url;
  unset($dbs['default']);

  foreach ($dbs as $key => $db) {
    $form['backup_client_database_options']['dbs'][$key] = array(
      '#type' => 'fieldset',
      '#title' => t('Choose tables to backup for: %database', array('%database' => $key)),
      '#collapsible' => TRUE,
      '#collapsed' => (count($dbs) != 1),
    );

    $current = db_set_active($key);
    $tables = backup_client_get_table_list();
    foreach($tables as $table) {
      $options[$table['name'] .'|'. $table['type']] = $table['name'];
      if($table['type'] == 'VIEW') {
        $options[$table['name'] .'|'. $table['type']] .= ' '. t('(is a view not a table)');
      }
    }
    db_set_active($current);
    $form['backup_client_database_options']['dbs'][$key]['backup_client_db_tables_'. $key] = array(
      '#type' => 'checkboxes',
      '#default_value' => variable_get('backup_client_db_tables_'. $key, array_keys($options)),
      '#options' => $options,
    );
  }
}

/**
 * Validation function for backup directory path.
 */
function backup_client_add_token_forms(&$form, $parent) {

  $form[$parent]['tokens'] = array(
    '#type' => 'fieldset',
    '#title' => t('Available tokens'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form[$parent]['tokens']['tokens'] = array(
    '#value' => theme_token_help('backup'),
  );

  $form[$parent]['tokens']['reset'] = array(
    '#type' => 'submit',
    '#value' => t('Reset backup count'),
    '#submit' => array('backup_client_backup_counter_reset'),
  );

}

/**
 * Validation function for backup directory path.
 */
function backup_client_check_backup_directory($form_element) {

  $directory = $form_element['#value'];

  file_check_directory($directory, FILE_CREATE_DIRECTORY, $form_element['#parents'][0]);

  // This part of file_check_directory does not execute so we include it here.
  if (!is_file("$directory/.htaccess")) {
    $htaccess_lines = "SetHandler Drupal_Security_Do_Not_Remove_See_SA_2006_006\nOptions None\nOptions +FollowSymLinks";
    if (($fp = fopen("$directory/.htaccess", 'w')) && fputs($fp, $htaccess_lines)) {
      fclose($fp);
      chmod($directory .'/.htaccess', 0664);
    }
    else {
      $variables = array('%directory' => $directory, '!htaccess' => '<br />'. nl2br(check_plain($htaccess_lines)));
      form_set_error($form_element['#parents'][0], t("Security warning: Couldn't write .htaccess file. Please create a .htaccess file in your %directory directory which contains the following lines: <code>!htaccess</code>", $variables));
      watchdog('security', "Security warning: Couldn't write .htaccess file. Please create a .htaccess file in your %directory directory which contains the following lines: <code>!htaccess</code>", $variables, WATCHDOG_ERROR);
    }
  }

  return $form_element;
}

/**
 * Callback for reset counter submit button. Resets backup counter variable.
 */
function backup_client_backup_counter_reset($form, &$form_state) {
  variable_set('backup_client_backup_count', 1);
  drupal_set_message(t('Backup counter reset to 1.'));
}

/**
 * Returns settings page for cron options.
 */
function backup_client_cron_settings_page() {
  module_load_include('inc', 'backup_client', 'backup_client.helper');

  backup_client_check_advanced_help('backup_client', 'settings-cron');
  backup_client_check_for_cron_deletions();

  $output .= drupal_get_form('backup_client_cron_settings_form');
  return $output;
}

/**
 * Returns recurring backup form.
 */
function backup_client_cron_settings_form() {

  $form['backup_client_cron_backup_on'] = array(
  '#type' => 'checkbox',
  '#title' => t('Allow cron to create and/or remove website and database backups.'),
  '#default_value' => variable_get('backup_client_cron_backup_on', 0),
  '#description' => t('Each time cron.php is accessed - Backup Client will check to see if the time since the last file backup has exceeded the backup frequency. If so, a mysqldump of the data base will be made - followed by a file backup of the site. If removal options are set - these will also be performed.'),
  );

  // Adds collapsable UI to checkboxes.
  drupal_add_js('
var map = {
  "#edit-backup-client-cron-backup-on" : "#backup-option-wrapper",
  "#edit-backup-client-cron-create-backups" : "#backup_client_backup_day_frequency",
  "#edit-backup-client-cron-dump-database" : "#backup_client_dump_day_frequency",
  "#edit-backup-client-cron-remove-extra-backups" : "#edit-backup-client-number-of-backups-to-keep-wrapper",
  "#edit-backup-client-cron-remove-extra-mysqldumps" : "#edit-backup-client-number-of-mysqldumps-to-keep-wrapper"
};

$.each(map, function(checkbox, target) {
  // Initialize
  $(checkbox).is(":checked") ? $(target).show() : $(target).hide();
  // Listener
  $(checkbox).change(function() {  $(target).toggle("slow"); } );
});', 'inline', 'footer');

  if (variable_get('backup_client_cron_backup_on', 0)) {
    // Set notice of upcoming website backup
    if (backup_client_time_to_execute(backup_client_last_dump_time('tar_backup'), 'backup_client_backup_day_frequency') && variable_get('backup_client_cron_create_backups', 0)) {
      drupal_set_message(t('Next cron will backup website'), 'status', FALSE);
    }

    // Set notice of upcoming database backup
    if (backup_client_time_to_execute(backup_client_last_dump_time('mysqldump'), 'backup_client_dump_day_frequency') && variable_get('backup_client_cron_dump_database', 0)) {
      drupal_set_message(t('Next cron will backup database(s)'), 'status', FALSE);
    }
  }

  $form['backup_client_backup_options'] = array(
    '#type' => 'fieldset',
    '#title' => t('Backup options'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#prefix' => '<div id="backup-option-wrapper">',
    );

  $form['backup_client_backup_options']['backup_client_cron_create_backups'] = array(
  '#type' => 'checkbox',
  '#title' => t('Allow cron to create website backups.'),
  '#default_value' => variable_get('backup_client_cron_create_backups', 0),
  '#description' => t('Each website backup event first creates database backups and then a website backup tar file'),
  );

  backup_client_date_form($form, 'backup_client_backup_options', 'backup_client_backup_day_frequency');

  $form['backup_client_backup_options']['backup_client_cron_dump_database'] = array(
  '#type' => 'checkbox',
  '#title' => t('Allow cron to create database backups.'),
  '#default_value' => variable_get('backup_client_cron_dump_database', 0),
  '#description' => t('Note: a database backup will be automatically created with each website backup. Choose this option if you want additional or database backups only.'),
  );

  backup_client_date_form($form, 'backup_client_backup_options', 'backup_client_dump_day_frequency');

  $options = array(
      '1' => t('Only one'),
      '2' => t('Two'),
      '3' => t('Three'),
      '4' => t('Four'),
      '7' => t('Seven'),
      '15' => t('Fifteen'),
      '30' => t('Thirty'),
      '100' => t('One hundred'),
      );

  $form['backup_client_removal_options'] = array(
    '#type' => 'fieldset',
    '#title' => t('Removal options'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['backup_client_removal_options']['backup_client_cron_remove_extra_backups'] = array(
    '#type' => 'checkbox',
    '#title' => t('Allow cron to remove older website backups.'),
    '#default_value' => variable_get('backup_client_cron_remove_extra_backups', 0),
  );

  $form['backup_client_removal_options']['backup_client_number_of_backups_to_keep'] = array(
    '#type' => 'select',
    '#title' => t('Number of website backups to keep'),
    '#default_value' => variable_get('backup_client_number_of_backups_to_keep', '100'),
    '#options' => $options,
    '#prefix' => '<div class="bc-date-form">',
    '#suffix' => '</div>',
  );

  $form['backup_client_removal_options']['backup_client_cron_remove_extra_mysqldumps'] = array(
    '#type' => 'checkbox',
    '#title' => t('Allow cron to remove older database backups.'),
    '#default_value' => variable_get('backup_client_cron_remove_extra_mysqldumps', 0),
    '#description' => t('The option below is for `backups` not `files` since a single database backup event can create multiple files (if there is more than one database configured).'),
  );

  $form['backup_client_removal_options']['backup_client_number_of_mysqldumps_to_keep'] = array(
  '#type' => 'select',
  '#title' => t('Number of database backups to keep'),
  '#default_value' => variable_get('backup_client_number_of_mysqldumps_to_keep', '100'),
  '#options' => $options,
  '#prefix' => '<div class="bc-date-form">',
  '#suffix' => '</div></div>',
  );

  return system_settings_form($form);
}

/**
 * Creates recurring date form.
 */
function backup_client_date_form(&$form, $parent, $key) {

  $form[$parent][$key .'div_begin'] = array('#value' => '<div class="bc-date-form" id="'. $key .'">');

  $form[$parent][$key .'_weekdays'] = array(
    '#type' => 'checkboxes',
    '#title' => t('On'),
    '#default_value' => variable_get($key .'_weekdays', array()),
    '#options' => array(
      'Mon' => t('Mon'),
      'Tue' => t('Tue'),
      'Wed' => t('Wed'),
      'Thu' => t('Thr'),
      'Fri' => t('Fri'),
      'Sat' => t('Sat'),
      'Sun' => t('Sun'),
      ),
    );

  $form[$parent][$key .'_weeks'] = array(
    '#type' => 'select',
    '#title' => t('Every'),
    '#default_value' => variable_get($key .'_weeks', '+0 week'),
    '#options' => array(
      '+0 week' => t('week'),
      '+1 week' => t('other week'),
      '+2 week' => t('third week'),
      '+3 week' => t('fourth week'),
      ),
    );

  $form[$parent][$key .'_hour'] = array(
    '#type' => 'select',
    '#title' => t('At'),
    '#default_value' => variable_get($key .'_hour', '12AM'),
    '#options' => array(
      '00' => t('12 AM'),
      '01' => t('1 AM'),
      '02' => t('2 AM'),
      '03' => t('3 AM'),
      '04' => t('4 AM'),
      '05' => t('5 AM'),
      '06' => t('6 AM'),
      '07' => t('7 AM'),
      '08' => t('8 AM'),
      '09' => t('9 AM'),
      '10' => t('10 AM'),
      '11' => t('11 AM'),
      '12' => t('12 PM'),
      '13' => t('1 PM'),
      '14' => t('2 PM'),
      '15' => t('3 PM'),
      '16' => t('4 PM'),
      '17' => t('5 PM'),
      '18' => t('6 PM'),
      '19' => t('7 PM'),
      '20' => t('8 PM'),
      '21' => t('9 PM'),
      '22' => t('10 PM'),
      '23' => t('11 PM'),
      ),
    );

  $form[$parent][$key .'div_end'] = array('#value' => '</div>');
}
