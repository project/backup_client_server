<?php
/**
 * @file
 * backup_client.module Backup Client v6.x provides tools to backup your
 * website and/or database locally
 * and externally.
 *
 * Core functions and hooks are contained in this file. All administration
 * forms are stored in backup_client.admin.inc and helper functions are in
 * backup_client.helper.inc.
 *
 * @see backup_client.admin.inc
 * @see backup_client.helper.inc
 * @see backup_client.install
 *
 * @ingroup backup Backup Client
 *
 * @todo Consider security of actions
 * @todo Check for mysql or mysqli - if not present give error.
 *
 * @todo Testing:
 *  - Backup and Dump
 *  - Change all settings
 *  - Via admin interface and cron
 *  - Test actions and triggers
 *  - Review help text
 *  - User permissions
 *  - Test removal of library components
 *
 * @todo Add drop box support:
 *   http://jaka.kubje.org/projects/dropbox-uploader/
 */

/** \addtogroup backup Backup Client */
/* @{ */

define('BACKUP_PATTERN', '\.tar\.gz|\.tar|\.sql\.gz|\.sql');
define('XML_RPC_REQUEST_BACKUP_TIME_LIMIT', 300);
define('DEFAULT_BACKUP_LOCATION', '/backup_client');

/**
 * Implementation of hook_perm().
 */
function backup_client_perm() {
  return (array('access backup client', 'create backups', 'administer backup client'));
}

/**
 * Implementation of hook_menu().
 */
function backup_client_menu() {

  $items['admin/build/backupclient'] = array(
    'title' => 'Backup Client',
    'page callback' => 'backup_client_tar_admin_page',
    'description' => 'Backup your files and database locally and/or externally.',
    'access arguments' => array('access backup client'),
    'file' => 'backup_client.admin.inc',
    'type' => MENU_NORMAL_ITEM,
  );
  $items['admin/build/backupclient/tar'] = array(
    'title' => 'Website',
    'page callback' => 'backup_client_tar_admin_page',
    'access arguments' => array('access backup client'),
    'weight' => 1,
    'file' => 'backup_client.admin.inc',
    'type' => MENU_DEFAULT_LOCAL_TASK,
  );
  $items['admin/build/backupclient/mysqldump'] = array(
    'title' => 'Database',
    'page callback' => 'backup_client_mysqldump_admin_page',
    'access arguments' => array('access backup client'),
    'file' => 'backup_client.admin.inc',
    'weight' => 2,
    'type' => MENU_LOCAL_TASK,
  );
  $items['admin/build/backupclient/configuration'] = array(
    'title' => 'Configuration',
    'page callback' => 'backup_client_general_settings_page',
    'access arguments' => array('administer backup client'),
    'weight' => 10,
    'file' => 'backup_client.admin.inc',
    'type' => MENU_LOCAL_TASK,
  );
  $items['admin/build/backupclient/configuration/general'] = array(
    'title' => 'General',
    'page callback' => 'backup_client_general_settings_page',
    'access arguments' => array('administer backup client'),
    'weight' => 1,
    'file' => 'backup_client.admin.inc',
    'type' => MENU_DEFAULT_LOCAL_TASK,
  );
  $items['admin/build/backupclient/configuration/cron'] = array(
    'title' => 'Recurring backups',
    'page callback' => 'backup_client_cron_settings_page',
    'access arguments' => array('administer backup client'),
    'weight' => 2,
    'file' => 'backup_client.admin.inc',
    'type' => MENU_LOCAL_TASK,
  );

  return $items;
}

/**
 * Exposing hook_backup_client for trigger.
 */
function backup_client_hook_info() {
  return array(
    'backup_client' => array(
      'backup_client' => array(
        'tar' => array(
          'runs when' => t('A website backup is made'),
        ),
        'mysqldump' => array(
          'runs when' => t('A database backup is made (this also happens when a website is backed up)'),
        ),
      ),
    ),
  );
}

/**
 * Adding hook_backup_client to system email and message actions.
 */
function backup_client_action_info_alter(&$actions) {
  $actions['system_send_email_action']['hooks']['backup_client'] = array('tar_backup', 'mysqldump');
  $actions['system_message_action']['hooks']['backup_client'] = array('tar_backup', 'mysqldump');
}

/**
 * Implementation of hook_action_info().
 */
function backup_client_action_info() {
  return array(
    'backup_client_backup_website_action' => array(
      'type' => 'system',
      'description' => t('Backup website and database'),
      'configurable' => FALSE,
      'hooks' => array(
        'nodeapi' => array('presave', 'insert', 'update', 'delete'),
      ),
    ),
    'backup_client_dump_database_action' => array(
      'type' => 'system',
      'description' => t('Backup database'),
      'configurable' => FALSE,
      'hooks' => array(
        'nodeapi' => array('presave', 'insert', 'update', 'delete'),
        'cron' => array('run'),
      ),
    ),
    'backup_client_remove_extra_backup_website_action' => array(
      'type' => 'system',
      'description' => t('Remove extra website backups'),
      'configurable' => FALSE,
      'hooks' => array(
        'nodeapi' => array('presave', 'insert', 'update', 'delete'),
      ),
    ),
    'backup_client_remove_extra_database_dumps_action' => array(
      'type' => 'system',
      'description' => t('Remove extra database dumps'),
      'configurable' => FALSE,
      'hooks' => array(
        'nodeapi' => array('presave', 'insert', 'update', 'delete'),
        'cron' => array('run'),
      ),
    ),
  );
}

/**
 * Action callback for backing up website.
 */
function backup_client_backup_website_action(&$object, $context = array()) {
  module_load_include('inc', 'backup_client', 'backup_client.helper');
  backup_client_make_tar_backup(array('verbose' => FALSE, 'note' => t('Backup triggered by action: %action', array('%action' => $context['op']))));
}

/**
 * Action callback for dumping database.
 */
function backup_client_dump_database_action(&$object, $context = array()) {
  module_load_include('inc', 'backup_client', 'backup_client.helper');
  backup_client_make_mysqldump(array('verbose' => FALSE, 'note' => t('Dump triggered by action: %action', array('%action' => $context['op']))));
}

/**
 * Action callback for removing extra website backups.
 */
function backup_client_remove_extra_backup_website_action(&$object, $context = array()) {
  module_load_include('inc', 'backup_client', 'backup_client.helper');
  backup_client_remove_extra_files('tar_backup', variable_get('backup_client_number_of_backups_to_keep', '100'));
}

/**
 * Action callback for removing extra database dumps.
 */
function backup_client_remove_extra_database_dumps_action(&$object, $context = array()) {
  module_load_include('inc', 'backup_client', 'backup_client.helper');
  backup_client_remove_extra_files('mysqldump', variable_get('backup_client_number_of_mysqldumps_to_keep', '100'));
}

/**
 * hook_backup_client - used to support trigger.
 */
function backup_client_backup_client($op, $parameters) {

  if (!in_array($op, array('tar_backup', 'mysqldump')) || !module_exists('trigger')) {
    return;
  }

  $aids = _trigger_get_hook_aids('backup_client', $op);
  $context = array(
    'hook' => 'backup_client',
    'op' => $op,
  );
  foreach ($aids as $aid => $action_info) {
    actions_do(array_keys($aids) , $parameters, $context);
  }
}

/**
 * Implementation of hook_requirements().
 */
function backup_client_requirements($phase) {

  if ($phase == 'runtime') {
    $last_backup = db_result(db_query("SELECT MAX(started) AS last FROM {backup_client} WHERE type = 'tar_backup'"));

    $last_dump = db_result(db_query("SELECT MAX(started) AS last FROM {backup_client} WHERE type = 'mysqldump'"));

    if (empty($last_backup) || empty($last_backup)) {
      $requirements['backup_client'] = array(
        'title' => 'Backup Client',
        'value' => t('No backup for this site exists'),
        'severity' => REQUIREMENT_WARNING,
        'description' => t('Backup Client has no records of a backup being made for this site. To backup your website goto the !admin_page.', array('!admin_page' => l(t('Backup Client - Website page') , 'admin/build/backupclient'))),
      );

      return $requirements;
    }

    // Find time since most recent of backup or dump.
    $time_since = min(time() - $last_backup, time() - $last_dump);

    $count_backup = db_result(db_query('SELECT COUNT(nid) FROM {node} WHERE changed > %d', $last_backup));
    $count_dump = db_result(db_query('SELECT COUNT(nid) FROM {node} WHERE changed > %d', $last_dump));

    if ($time_since < 60*60*24*7 && max($count_backup, $count_dump) == 0) {
      // Okay if less than one week
      $status = REQUIREMENT_OK;
    }
    elseif ($time_since < 60*60*24*30) {
      // Warning if less than 30 days
      $status = REQUIREMENT_WARNING;
    }
    else {
      // Error if more than a month
      $status = REQUIREMENT_ERROR;
    }

    $count_backup = db_result(db_query('SELECT COUNT(nid) FROM {node} WHERE changed > %d', $last_backup));
    $count_dump = db_result(db_query('SELECT COUNT(nid) FROM {node} WHERE changed > %d', $last_dump));

    $requirements['backup_client'] = array(
      'title' => 'Backup Client',
      'value' => t('Last backup of website was on: %time', array('%time' => date('Y-m-d i:hA', $last_backup))) .'<br />'. t('Nodes created or changed since then: %nodes', array('%nodes' => $count_backup)) .'<br />'. t('Last dump of database was on: %time', array('%time' => date('Y-m-d i:hA', $last_dump))) .'<br />'. t('Nodes created or changed since then: %nodes', array('%nodes' => $count_dump)),
      'severity' => $status,
      'description' => ($status <> REQUIREMENT_OK) ? t('You have nodes that are not backed up and/or it has been more than a week since you have made a backup.') .'<br />'. t('To backup your website goto the !admin_page.', array('!admin_page' => l(t('Backup Client - Website page') , 'admin/build/backupclient'))) : '',
    );

    return $requirements;
  }
}

/**
 * Implementation of hook_cron().
 */
function backup_client_cron() {
  global $db_url;
  module_load_include('inc', 'backup_client', 'backup_client.helper');

  variable_set('backup_client_cron_spacing', time() - variable_get('backup_client_last_cron', 0));
  variable_set('backup_client_last_cron', time());

  if (variable_get('backup_client_cron_backup_on', 0)) {

    // Handle scheduled website backups
    $last_backup_time = backup_client_last_dump_time('tar_backup');
    if (backup_client_time_to_execute($last_backup_time, 'backup_client_backup_day_frequency') && variable_get('backup_client_cron_create_backups', 0)) {
      backup_client_make_tar_backup(
        array(
          'note' => t('Cron triggered backup'),
          'gzip_tar' => variable_get('backup_client_gzip_tar_file', 1),
          'add_info' => variable_get('backup_client_add_info', 0),
          'only_recent_sql' => variable_get('backup_client_only_recent_mysql', 0),
          'verbose' => FALSE,
        )
      );
    }

    // Remove extra website backups if necessary
    if (variable_get('backup_client_cron_remove_extra_backups', 0)) {
      backup_client_remove_extra_files('tar_backup', variable_get('backup_client_number_of_backups_to_keep', '100'));
    }

    // Handle scheduled mysql dumps
    $last_dump_time = backup_client_last_dump_time('mysqldump');
    if (backup_client_time_to_execute($last_dump_time, 'backup_client_dump_day_frequency')  && variable_get('backup_client_cron_dump_database', 0)) {

      // Normalize $db_url to an array and check for multiple database dump
      if (!is_array($db_url) || !variable_get('backup_client_dump_all_databases', TRUE)) {
        $databases = array('default');
      }
      else {
        $databases = array_keys($db_url);
      }

      // Dump database before tar'ing site.
      // Loop through databases
      $bid = backup_client_next_bid();
      foreach ($databases as $database) {
        backup_client_make_mysqldump(
          array(
            'bid' => $bid,
            'note' => t('Cron triggered dump'),
            'database' => $database,
          )
        );
      }
    }

    // Remove extra database dumps if necessary.
    if (variable_get('backup_client_cron_remove_extra_mysqldumps', 0)) {
      backup_client_remove_extra_files('mysqldump', variable_get('backup_client_number_of_mysqldumps_to_keep', '100'));
    }
  }
}

/**
 * Implementation of hook_token_list().
 */
function backup_client_token_list($type = 'all') {

  if ($type == 'backup' || $type == 'all' || $type == 'global') {
    $tokens['backup']['random-characters']  = t("Five random characters: %random", array('%random' => user_password(5)));
    $tokens['backup']['backup-database-name']  = t("Database name - filename friendly: %database", array('%database' => backup_client_filename_friendly(backup_client_sql_databasename())));
    $tokens['backup']['backup-site-name']  = t("Site name - filename friendly: %sitename", array('%sitename' => backup_client_filename_friendly(variable_get('site_name', 'Drupal'))));
    $tokens['backup']['backup-site-url']  = t("Site url - filename friendly: %siteurl", array('%siteurl' => backup_client_filename_friendly(backup_client_site_url())));
    $tokens['backup']['backup-site-timestamp']  = t("Site timestamp with minute granularity: %timestamp", array('%timestamp' => date('Y-m-d-H-i')));
    $tokens['backup']['backup-count']  = t('Number of backups created by backup client module with zero padding (ex. 00123)');
  }

  return $tokens;
}

/**
 * Implementation of hook_token_values().
 */
function backup_client_token_values($type, $object = NULL) {

  $values = array();
  switch ($type) {
    case 'all':
    case 'backup':
    case 'global':
      $values['random-characters'] = user_password(5);
      $values['backup-database-name'] = backup_client_filename_friendly(backup_client_sql_databasename());
      $values['backup-site-name'] = backup_client_filename_friendly(variable_get('site_name', 'Drupal'));
      $values['backup-site-url'] = backup_client_filename_friendly(backup_client_site_url());
      $values['backup-site-timestamp'] = date('Y-m-d-H-i');
      $values['backup-count'] = sprintf("%05s", variable_get('backup_client_backup_count', 1));
  }
  return $values;
}

/**
 * Converts string to file name friendly format.
 *
 * @todo Check for transliteration module and use if available.
 */
function backup_client_filename_friendly($string) {

  return drupal_strtolower(preg_replace('([^[:alnum:]._])', '-', $string));
}

/**
 * Returns database name.
 */
function backup_client_sql_databasename() {

  return db_result(db_query('SELECT database()'));
}

/**
 * Returns trimmed URL of website.
 */
function backup_client_site_url() {
  global $base_url;

  return str_replace(array('http://', 'https://') , '', $base_url);
}

/* }@ */
