<?php

/**
 * @file
 *  backup_client.drush.inc Drush commands for backup_client module
 *
 * @see backup_client.module
 *
 * @ingroup backup
 */

/** \addtogroup backup Backup Client */
/* @{ */

/**
 * Implementation of hook_drush_help().
 */
function backup_client_drush_help($section) {
  switch ($section) {
    case 'drush:bc backup':
      return dt("Backup the website and database.");
    case 'drush:bc multidump':
      return dt("Backup the database(s).");
  }
}

/**
 * Implementation of hook_drush_command().
 */
function backup_client_drush_command() {
  $items['bc backup'] = array(
    'callback' => 'backup_client_drush_backup',
    'description' => dt('Backup the website and database.'),
    'examples' => array(
      'drush bc backup' => 'Backup the website and database.',
      'drush bc backup "Backup before upgrade"' => 'Backup the website and database and attach a note.',
    ),
    'arguments' => array(
      'note'       => "Optional. A note describing this backup.",
    ),
  );

  $items['bc mysqldump'] = array(
    'callback' => 'backup_client_drush_mysqldump',
    'description' => dt('Dump drupal\'s database(s).'),
    'examples' => array(
      'drush bc mysqldump' => 'Backup the website and database.',
    ),
    'arguments' => array(
      'note'       => "Optional. A note describing dump.",
    ),
  );

  return $items;
}

/**
 * Implementation of `drush bc backup`.
 *
 * @todo Token for url returns different value when run under Drush.
 */
function backup_client_drush_backup($note = '') {
  include_once(drupal_get_path('module', 'backup_client') .'/backup_client.helper.inc');

  drush_print('Starting backup...');

  $bid = backup_client_make_tar_backup(array('note' => $note ? $note : t('Drush triggered backup')));
  if ($bid) {
    $result = db_query("SELECT filepath FROM {backup_client} WHERE bid = %d", $bid);
    while ($file = db_fetch_object($result)) {
      drush_print('Created: '. $file->filepath);
    }
    drush_print('Success!');
  }
  else {
    drush_print('Fail!');
  }
}

/**
 * Implementation of `drush bc mysqldump`.
 */
function backup_client_drush_mysqldump($note = '') {
  global $db_url;

  include_once(drupal_get_path('module', 'backup_client') .'/backup_client.helper.inc');

  drush_print(dt('Starting mysqldump...'));

  $bid = backup_client_next_bid();

  // Normalize $db_url to an array and check for multiple database dump
  if (!is_array($db_url) || !variable_get('backup_client_dump_all_databases', TRUE)) {
    $databases = array('default');
  }
  else {
    $databases = array_keys($db_url);
  }

  // Dump database before tar'ing site.
  // Loop through databases
  foreach ($databases as $database) {
    backup_client_make_mysqldump(
      array(
        'bid' => $bid,
        'note' => $note ? $note : t('Drush triggered dump'),
        'database' => $database,
      )
    );
  }

  $result = db_query("SELECT filepath FROM {backup_client} WHERE bid = %d", $bid);
  while ($file = db_fetch_object($result)) {
    drush_print('Created: '. $file->filepath);
  }

  drush_print('Finished!');
}
/* }@ */
