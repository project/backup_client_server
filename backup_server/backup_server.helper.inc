<?php
/**
 * @file
 * backup_server.helper.inc Provides all the functions required to
 * support the backup client server module.
 *
 * @see backup_server.module
 *
 * @ingroup backup
 */

/** \addtogroup backup Backup Client */
/* @{ */

/**
 * Removes extra backups for web service call.
 *
 * @param $sid
 *   Site ID.
 * @param $redirect
 *   The path to return to once completed.
 */
function backup_server_update($sid, $redirect = 'admin/build/backupserver') {

  $site = db_fetch_object(db_query('SELECT * FROM {backup_server_sites} WHERE sid = %d', $sid));

  $url = $site->url .'/xmlrpc.php';
  $method = 'backup.info';

  // Get info on backups.
  $response = xmlrpc($url, $method, $site->username, $site->password, 0);
  // dump(array($url, $method, $site->username, $site->password, 0, $response));

  if (!$response) {
    drupal_set_message(t('<strong>!site_name</strong> :: error connecting to server - check your settings', array('!site_name' => $site->name)), 'error');
  }

  // Get node and user info
  if (isset($response['counts'])) {
    $added = 0;
    db_query('DELETE FROM {backup_server_counts} WHERE sid = %d', $sid);

    foreach ($response['counts'] as $type => $values) {
      foreach ($values as $date => $value) {
        db_query("INSERT INTO {backup_server_counts} (sid, type, date, count) VALUES (%d, '%s', %d, %d)", $sid, $type, $date, $value);
        ++$added;
      }
      $message[] = t('!type %added', array('!type' => ucfirst(str_replace('-', ' ', $type)), '%added' => $added));
      $added = 0;
    }
  }

  // Get mysql info
  if (isset($response['mysql_stat'])) {
    $added = 0;
    db_query('DELETE FROM {backup_server_mysql} WHERE sid = %d', $sid);

    foreach ($response['mysql_stat'] as $name => $value) {
      db_query("REPLACE INTO {backup_server_mysql} (sid, name, value) VALUES (%d, '%s', '%s')", $sid, $name, $value);
      ++$added;
    }

    $message[] = t('MySQL %added', array('%added' => $added));
  }

  // Get module info
  if (isset($response['modules'])) {
    db_query('DELETE FROM {backup_server_modules} WHERE sid = %d', $sid);

    $data = $response['modules'];
    // Move 'drupal' to the top.
    $data = array('drupal' => $data['drupal']) + $data;

    $added = 0;
    foreach ($data as $key => $project) {

      db_query("REPLACE INTO {backup_server_modules} (sid, module, name, recommended, existing, existing_link, release_link, status, date) VALUES (%d, '%s', '%s', '%s', '%s', '%s', '%s', %d, %d)", $sid, $key, $project['title'], $project['recommended'], $project['existing_version'], $project['releases'][$project['existing_version']]['release_link'], $project['releases'][$project['recommended']]['release_link'], $project['status'], $project['datestamp']);

      ++$added;
    }

    $message[] = t('Modules %added', array('%added' => $added));
  }

  // Get backup file info.
  if (isset($response['backups'])) {
    db_query('DELETE FROM {backup_server_backups} WHERE sid = %d', $sid);

    $fields = array('id', 'bid', 'type', 'filename', 'filepath', 'note', 'started', 'duration', 'size', 'status', 'sid');
    $placeholders = "%d, %d, '%s', '%s', '%s', '%s', %d, %d, %d, %d, %d";

    $added = 0;
    foreach ($response['backups'] as $backup) {
      $backup['sid'] = $sid;

      db_query('REPLACE INTO {backup_server_backups} ('. implode(', ', $fields) .') VALUES ('. $placeholders .')', $backup);

      ++$added;
    }

    $message[] = t('Backups %added', array('%added' => $added));
  }

  // Get settings info.
  if (isset($response['settings'])) {
    db_query('DELETE FROM {backup_server_settings} WHERE sid = %d', $sid);
    $added = 0;

    foreach ($response['settings'] as $key => $value) {
      if (is_array($value)) {
        $value = implode(',', array_filter($value));
      }

      db_query("INSERT INTO {backup_server_settings} (sid, name, value) VALUES (%d, '%s', '%s')", $sid, $key, $value);
      ++$added;
    }

    $message[] = t('Settings %added', array('%added' => $added));
  }

  // Get server info.
  if (isset($response['server'])) {
    db_query('DELETE FROM {backup_server_config} WHERE sid = %d', $sid);
    $added = 0;

    foreach ($response['server'] as $key => $value) {

      // Collapse array information.
      if (is_array($value)) {
        $value = implode(', ', array_filter($value));
      }

      db_query("REPLACE INTO {backup_server_config} (sid, name, value) VALUES (%d, '%s', '%s')", $sid, $key, $value);
      ++$added;
    }

    $message[] = t('Config %added', array('%site' => $site->name, '%added' => $added));
  }

  // Get requirements info
  if (isset($response['requirements'])) {

    // Add in a few more requirements
    backup_server_requirements_add($response['requirements'], $sid);

    $added = 0;
    db_query('DELETE FROM {backup_server_requirements} WHERE sid = %d', $sid);

    foreach ($response['requirements'] as $data) {
      db_query("INSERT INTO {backup_server_requirements} (sid, title, value, severity, description, weight) VALUES (%d, '%s', '%s', '%s', '%s', %d)", $sid, $data['title'], $data['value'], $data['severity'], $data['description'], $data['weight']);
      ++$added;
    }

    $message[] = t('Requirements %added', array('%added' => $added));
  }

  // Set message
  if (count($message)) {
    drupal_set_message(t('<strong>!site_name</strong> :: !messages', array('!site_name' => $site->name, '!messages' => implode(' | ', $message))));
  }

  // Update last update time.
  db_query('UPDATE {backup_server_sites} SET last = %d WHERE sid = %d', time(), $sid);

  if ($redirect != '') {
    drupal_goto($redirect);
  }
}

/**
 * Builds page of backup data.
 *
 * @param $sid
 *   Site ID.
 * @param $filter
 *   The type of backup data to show.
 * @returns
 *   Page of backup data.
 */
function backup_server_backups_page($sid, $filter = FALSE) {

  $site = db_fetch_object(db_query('SELECT name, last, url FROM {backup_server_sites} WHERE sid = %d', $sid));

  if ($site->name == '') {
    drupal_set_message(t('Cannot find website.'), 'error');
    return '';
  }

  drupal_set_title($site->name);

  $pager_limit = variable_get('backup_client_pager_number', '10');

  $header = array(
    array('data' => t('#') , 'field' => 'bid'),
    array('data' => t('Filename') , 'field' => 'filename'),
    array('data' => t('Type') , 'field' => 'type'),
    array('data' => t('Size') , 'field' => 'size'),
    array('data' => t('Date') , 'field' => 'started', 'sort' => 'desc'),
    array('data' => t('Duration<br />(sec)') , 'field' => 'duration'),
    array('data' => t('Note') , 'field' => 'note'),
  );

  $filters = db_query("SELECT DISTINCT(type) FROM {backup_server_backups} WHERE sid = %d", $sid);

  $filter_links = array();
  while ($filter_option = db_result($filters)) {
    $filter_options[] = $filter_option;
    $link = l($filter_option, 'admin/build/backupserver/site/'. $sid .'/list/'. $filter_option);
    if ($filter_option == $filter) {
      $link = '<strong>'. $link .'</strong>';
    }
    $filter_links[] = $link;
  }

  if (!$filter || $filter == 'list') {
    $link = '<strong>'. t('all') .'</strong>';
  }
  else {
    $link = t('all');
  }

  array_unshift($filter_links, l($link, 'admin/build/backupserver/site/'. $sid .'/list', array('html' => TRUE)));
  $output = implode(' | ', $filter_links);

  if ($filter) {
    if (in_array($filter, $filter_options)) {
      $sql_addition = " AND type = '". $filter ."'";
    }
    else {
      drupal_set_message(t('Files do not exist for: !filter', array('!filter' => $filter)), 'error');
      return $output;
    }
  }

  $sql = "SELECT * FROM {backup_server_backups} WHERE sid = ". (int) $sid . $sql_addition;

  $result = pager_query($sql . tablesort_sql($header), $pager_limit);
  while ($file = db_fetch_object($result)) {

    // If not absolute URL - use site's URL
    if (strpos($file->filepath, 'http') !== 0) {
      $file->filepath = $site->url .'/'. $file->filepath;
    }

    $rows[] = array(
      $file->bid,
      '<a href="'. check_plain($file->filepath) .'">'. check_plain($file->filename) .'</a>',
      $file->type,
      format_size($file->size),
      date('Y-m-d h:iA', $file->started),
      $file->duration,
      check_plain($file->note),
    );
  }

  if ($rows) {
    $pager = theme('pager', NULL, $pager_limit, 0);
    if (!empty($pager)) {
      $rows[] = array(array('data' => $pager, 'colspan' => count($rows[0])));
    }
    $output .= theme('table', $header, $rows);
  }
  else {
    $output = t('No backups found.');
  }

  if (backup_server_remote_backup_allowed($sid)) {

    $output .= drupal_get_form('backup_server_trigger_backup', $sid);
  }

  return $output;

}

function backup_server_remote_backup_allowed($sid) {
  $enabled = db_result(db_query("SELECT value FROM {backup_server_settings} WHERE sid = %d AND name = 'backup_client_service_allow_external_access'", $sid));

  $backup = db_result(db_query("SELECT value FROM {backup_server_settings} WHERE sid = %d AND name = 'backup_client_service_allow_external_backup'", $sid));

  return $enabled && $backup;
}

function backup_server_trigger_backup($temp, $sid) {

  $form['sid'] = array(
    '#type' => 'hidden',
    '#value' => $sid,
  );

  $form['backup'] = array(
    '#type' => 'submit',
    '#value' => t('Trigger website backup'),
  );

  return $form;
}

function backup_server_trigger_backup_submit($form, &$form_state) {

  // This can take a long time.
  module_load_include('inc', 'backup_client', 'backup_client.helper');
  backup_client_expand_environment();

  $sid = $form_state['values']['sid'];

  $site = db_fetch_object(db_query('SELECT * FROM {backup_server_sites} WHERE sid = %d', $sid));

  $url = $site->url .'/xmlrpc.php';
  $method = 'backup.website';

  // Get info on backups.
  $response = xmlrpc($url, $method, $site->username, $site->password);

  if (!$response) {
    drupal_set_message(t('<strong>!site_name</strong> :: error connecting to server - check your settings', array('!site_name' => $site->name)), 'error');
    return;
  }

  // Update backup table with response.
  $fields = array('id', 'bid', 'type', 'filename', 'filepath', 'note', 'started', 'duration', 'size', 'status', 'sid');
  $placeholders = "%d, %d, '%s', '%s', '%s', '%s', %d, %d, %d, %d, %d";

  $added = 0;

  foreach ($response['backups'] as $backup) {
    $backup['sid'] = $sid;

    db_query('INSERT INTO {backup_server_backups} ('. implode(', ', $fields) .') VALUES ('. $placeholders .')', $backup);

    ++$added;
  }

  drupal_set_message(t('Backup files added: %added', array('%added' => $added)));
}

/**
 * Builds page of settings data.
 *
 * @param $sid
 *   Site ID.
 * @returns
 *   Page of settings data.
 */
function backup_server_settings_page($sid) {
  $site = db_fetch_object(db_query('SELECT name, last FROM {backup_server_sites} WHERE sid = %d', $sid));

  if ($site->name == '') {
    drupal_set_message(t('Cannot find website.'), 'error');
    return '';
  }

  drupal_set_title($site->name);

  $pager_limit = 75;

  $header = array(
    array('data' => t('Name') , 'field' => 'name', 'sort' => 'desc'),
    array('data' => t('Value') , 'field' => 'value'),
  );

  $sql = "SELECT name, value FROM {backup_server_settings} WHERE sid = ". (int) $sid;
  $result = pager_query($sql . tablesort_sql($header), $pager_limit);
  while ($setting = db_fetch_object($result)) {
    $rows[] = array(
      $setting->name,
      $setting->value,
    );
  }

  if ($rows) {
    $pager = theme('pager', NULL, $pager_limit, 0);
    if (!empty($pager)) {
      $rows[] = array(array('data' => $pager, 'colspan' => count($rows[0])));
    }
    $output .= theme('table', $header, $rows);
  }
  else {
    $output .= t('No settings found.');
  }

  return $output;
}

function backup_server_counts_page($sid) {
  $site = db_fetch_object(db_query('SELECT name, last FROM {backup_server_sites} WHERE sid = %d', $sid));

  if ($site->name == '') {
    drupal_set_message(t('Cannot find website.'), 'error');
    return '';
  }

  drupal_set_title($site->name);

  $types = db_query("SELECT DISTINCT(type) FROM {backup_server_counts} WHERE sid = %d", $sid);

  while ($type = db_result($types)) {
    $title = ucfirst(str_replace('-', ' ', $type));

    $year_ago = time() - 60*60*24*370;

    $result = db_query("SELECT date, count FROM {backup_server_counts} WHERE sid = %d AND type = '%s' AND date > %d ORDER BY date DESC", $sid, $type, $year_ago);

    $values = array();
    while ($value = db_fetch_object($result)) {
      $values[] = $value->count;
    }

    // Pad values out to one year
    while (count($values) <= 52) {
      $values[] = 0;
    }

    $params['cht'] = 'bvs'; // Vertical bar chart.
    $params['chtt'] = $title; // Set title.
    $params['chbh'] = 'a'; // Automatic bar with resizing.

    $width = round(15 * count($values));
    $params['chs'] = "{$width}x200"; // Width of image.

    $params['chd'] = backup_server_gchart_encode($values); // Encoded values.
    $params['chco'] = '5895BE'; // Bar color;

    $ymax = max($values);
    $yinterval = (int) ($max / 8);
    if ($yinterval == 0) {
      $yinterval = 1;
    }

    $xmax = count($values);
    $params['chxt'] = 'x,x,y';
    $params['chxr'] = "2,0,$ymax";
    $params['chxl'] = '1:|('. t('Weeks ago') .')|';
    $params['chxp'] = '1,50';

    // Build string array to collapse into parameters.
    $string = array();
    foreach ($params as $key => $value) {
      $string[] = $key .'='. $value;
    }

    $url = 'http://chart.apis.google.com/chart?';
    $output .= '<p>'. '<img src="'. $url . implode('&amp;', $string) .'" /></p>';
  }

  if ($output == '') {
    $output = t('No data to report.');
  }

  return $output;
}

function backup_server_gchart_encode($values) {
  $max = max($values);

  $encoding = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  $str = 's:';
  $t = count($values);
  for ($i = 0; $i < $t; $i++) {
    $current_value = $values[$i];
    if ((drupal_strlen($current_value) != 0) && ($current_value >= 0) && ($max != 0)) {
      $str .= drupal_substr($encoding, round((drupal_strlen($encoding)-1) * $current_value / $max), 1);
    }
    else {
      $str .= '_';
    }
  }
  
  return $str;
}

/**
 * Builds page of config data.
 *
 * @param $sid
 *   Site ID.
 * @returns
 *   Page of config data.
 */
function backup_server_config_page($sid) {
  $site = db_fetch_object(db_query('SELECT name, last FROM {backup_server_sites} WHERE sid = %d', $sid));

  if ($site->name == '') {
    drupal_set_message(t('Cannot find website.'), 'error');
    return '';
  }

  drupal_set_title($site->name);

  $pager_limit = 75;

  $header = array(
    array('data' => t('Name') , 'field' => 'name', 'sort' => 'asc'),
    array('data' => t('Value') , 'field' => 'value'),
  );

  $sql = "SELECT name, value FROM {backup_server_config} WHERE sid = ". (int) $sid;
  $result = pager_query($sql . tablesort_sql($header), $pager_limit);
  while ($setting = db_fetch_object($result)) {
    $rows[] = array(
      $setting->name,
      $setting->value,
    );
  }

  if ($rows) {
    $pager = theme('pager', NULL, $pager_limit, 0);
    if (!empty($pager)) {
      $rows[] = array(array('data' => $pager, 'colspan' => count($rows[0])));
    }
    $output .= theme('table', $header, $rows);
  }
  else {
    $output .= t('No settings found.');
  }

  return $output;
}

/**
 * Builds page of mysql data.
 *
 * @param $sid
 *   Site ID.
 * @returns
 *   Page of mysql data.
 */
function backup_server_mysql_page($sid) {
  $site = db_fetch_object(db_query('SELECT name, last FROM {backup_server_sites} WHERE sid = %d', $sid));

  if ($site->name == '') {
    drupal_set_message(t('Cannot find website.'), 'error');
    return '';
  }

  drupal_set_title($site->name);

  $pager_limit = 75;

  $header = array(
    array('data' => t('Name') , 'field' => 'name', 'sort' => 'asc'),
    array('data' => t('Value') , 'field' => 'value'),
  );

  $sql = "SELECT name, value FROM {backup_server_mysql} WHERE sid = ". (int) $sid;
  $result = pager_query($sql . tablesort_sql($header), $pager_limit);
  while ($setting = db_fetch_object($result)) {
    $rows[] = array(
      $setting->name,
      $setting->value,
    );
  }

  if ($rows) {
    $pager = theme('pager', NULL, $pager_limit, 0);
    if (!empty($pager)) {
      $rows[] = array(array('data' => $pager, 'colspan' => count($rows[0])));
    }
    $output .= theme('table', $header, $rows);
  }
  else {
    $output .= t('No settings found.');
  }

  return $output;
}

/**
 * Builds page of mysql data.
 *
 * @param $status
 *   The status value of a module.
 * @returns
 *   HTML of status image.
 */
function backup_server_module_status_image($status, $message_text = FALSE) {

  $status_map = array(
    -1 => 'Update Not Checked',
    -2 => 'Update Unknown',
    -3 => 'Update Not Fetched',
    1 => 'Update Not Secure',
    2 => 'Update Revoked',
    3 => 'Update Not Supported',
    4 => 'Update Not Current',
    5 => 'Update Current',
  );

  $message = $status_map[$status];

  if ($status >= 5) {
    $output = '<img src="'. url('misc/watchdog-ok.png') . '" title="'. $message .'" />';
  }

  if ($status == 4) {
    $output = '<img src="'. url('misc/watchdog-warning.png') . '" title="'. $message .'" /> ';
  }

  if ($status < 4) {
    $output = '<img src="'. url('misc/watchdog-error.png') . '" title="'. $message .'" /> ';
  }

  if ($message_text) {
    $output .= ' '. $message;
  }

  return $output;
}

/**
 * Builds page of mysql data.
 *
 * @param $status
 *   The status value of a module.
 * @returns
 *   HTML of status image.
 */
function backup_server_system_status_image($status) {

  if ($status == 2) {
    $output = '<img src="'. url('misc/watchdog-error.png') . '" /> ';
  }
  elseif ($status == 1) {
    $output = '<img src="'. url('misc/watchdog-warning.png') . '" />';
  }
  else {
    $output = '<img src="'. url('misc/watchdog-ok.png') . '" />';
  }

  return $output;
}

/**
 * Builds page of module data.
 *
 * @param $sid
 *   Site ID.
 * @returns
 *   Page of module data.
 */
function backup_server_module_page($sid) {
  $site = db_fetch_object(db_query('SELECT name, last FROM {backup_server_sites} WHERE sid = %d', $sid));

  if ($site->name == '') {
    drupal_set_message(t('Cannot find website.'), 'error');
    return '';
  }

  drupal_set_title($site->name);

  $pager_limit = 75;

  $header = array(
    array('data' => t('Name') , 'field' => 'name'),
    array('data' => t('Current') , 'field' => 'existing'),
    array('data' => t('Recommended') , 'field' => 'recommended'),
    array('data' => t('Date') , 'field' => 'date'),
    array('data' => t('Status') , 'field' => 'status', 'sort' => 'asc'),
  );

  $sql = "SELECT * FROM {backup_server_modules} WHERE sid = ". (int) $sid;
  $result = pager_query($sql . tablesort_sql($header), $pager_limit);
  while ($module = db_fetch_object($result)) {
    $rows[] = array(
      '<a href="http://drupal.org/project/'. $module->module .'">'. $module->name . '</a>',
      '<a href="'. $module->existing_link .'" target="_blank">'. $module->existing . '</a>',
      '<a href="'. $module->release_link .'" target="_blank">'. $module->recommended . '</a>',
      date('Y-m-d', $module->date),
      backup_server_module_status_image($module->status, TRUE),
    );
  }

  if ($rows) {
    $pager = theme('pager', NULL, $pager_limit, 0);
    if (!empty($pager)) {
      $rows[] = array(array('data' => $pager, 'colspan' => count($rows[0])));
    }
    $output .= theme('table', $header, $rows);
  }
  else {
    $output .= t('No settings found.');
  }

  return $output;
}

/**
 * Generates status image given a type of backup and site ID.
 *
 * @param $sid
 *   Site ID.
 * @param $type
 *   Type of backups.
 * @returns
 *   HTML of image status.
 */
function backup_server_status_backups($sid, $type, $warning, $error) {

  if (!backup_server_module_test($sid, $type)) {
    return t('No backups');
  }

  $backup_date = db_result(db_query("SELECT MAX(started) FROM {backup_server_backups} WHERE sid = %d AND type = '%s'", $sid, $type));

  $now = time();
  $warning_time = 60*60*24* $warning;
  $error_time = 60*60*24* $error;

  if ($backup_date > ($now - $warning_time)) {
    $title = t('Less than !warning days.', array('!warning' => variable_get("backup_server_{$type}_warning", '3')));
    $image = '<img src="'. url('misc/watchdog-ok.png') . '" title="'. $title .'" />';
  }
  elseif ($backup_date > ($now - $error_time)) {
    $title = t('More than !warning and less than !error days.', array('!warning' => variable_get("backup_server_{$type}_warning", '3'), '!error' => variable_get("backup_server_{$type}_error", '7')));
    $image = '<img src="'. url('misc/watchdog-warning.png') . '" title="'. $title .'" />';
  }
  else {
    $title = t('More than !error days.', array('!error' => variable_get("backup_server_{$type}_error", '7')));
    $image = '<img src="'. url('misc/watchdog-error.png') . '" title="'. $title .'" />';
  }

  return l($image, 'admin/build/backupserver/site/'. $sid .'/list/'. $type, array('html' => TRUE));
}

function backup_server_module_test($sid, $type) {
  $map = array(
    'tar_backup' => 'backup_client',
    's3_backup' => 'backup_client_s3',
  );

  $count = db_result(db_query("SELECT COUNT(sid) AS count FROM {backup_server_modules} WHERE sid = %d AND module = '%s'", $sid, $map[$type]));

  return $count;

}

/**
 * Generates status image for all modules given a site ID.
 *
 * @param $sid
 *   Site ID.
 * @returns
 *   HTML of linked image status.
 */
function backup_server_status_modules($sid) {
  $status = db_result(db_query('SELECT MIN(status) FROM {backup_server_modules} WHERE sid = %d', $sid));

  return l(backup_server_module_status_image($status), 'admin/build/backupserver/site/'. $sid .'/modules', array('html' => TRUE));
}

/**
 * Generates status image for all requirements given a site ID.
 *
 * @param $sid
 *   Site ID.
 * @returns
 *   HTML of linked image status.
 */
function backup_server_status_requirements($sid) {
  $status = db_fetch_object(db_query('SELECT MAX(severity) as severity, COUNT(sid) as count FROM {backup_server_requirements} WHERE sid = %d', $sid));

  // If count is low then there is an update error
  if ($status->count < 10) {
    $status->severity = 2;
  }

  return l(backup_server_system_status_image($status->severity), 'admin/build/backupserver/site/'. $sid, array('html' => TRUE));
}

/**
 * Builds page of Drupal status data.
 *
 * @param $sid
 *   Site ID.
 * @returns
 *   Page of Drupal status.
 */
function backup_server_requirements_page($sid) {
  $site = db_fetch_object(db_query('SELECT name, last, url FROM {backup_server_sites} WHERE sid = %d', $sid));

  if ($site->name == '') {
    drupal_set_message(t('Cannot find website.'), 'error');
    return '';
  }

  drupal_set_title($site->name);

  // Load .install files
  include_once './includes/install.inc';

  $result = db_query('SELECT * FROM {backup_server_requirements} WHERE sid = %d', $sid);

  while ($status = db_fetch_array($result)) {
    $requirements[] = $status;
  }

  if (count($requirements)) {
    usort($requirements, '_system_sort_requirements');
    $output = theme('status_report', $requirements);
  }
  else {
    drupal_set_message(t('No status information found.'), 'error');
    $output = '<p>'. t('Please check your backup service and server settings.') .'</p>';
  }

  return $output;
}

/**
 * Adds additional requirements.
 *
 * @param $requirements
 *   Drupal requirements array.
 * @param $sid
 *   Site ID.
 */
function backup_server_requirements_add(&$requirements, $sid) {

  $requirements[] = backup_server_param_status($sid, array(
    'table' => 'backup_server_config',
    'param' => array('column' => 'name', 'value' => 'php_memory_limit'),
    'value' => array('column' => 'value'),
    'limits' => array('warning' => 256, 'error' => 512),
    'order' => 'less than',
  ));

  $requirements[] = backup_server_param_status($sid, array(
    'table' => 'backup_server_config',
    'param' => array('column' => 'name', 'value' => 'php_max_execution_time'),
    'value' => array('column' => 'value'),
    'limits' => array('warning' => 91, 'error' => 120),
    'order' => 'less than',
  ));

  $requirements[] = backup_server_param_status($sid, array(
    'table' => 'backup_server_config',
    'param' => array('column' => 'name', 'value' => 'disk_free_space'),
    'value' => array('column' => 'value'),
    'limits' => array('warning' => 50000000000, 'error' => 5000000000),
    'formatter' => 'format_size',
  ));
  
  $requirements[] = backup_server_param_status($sid, array(
    'table' => 'backup_server_config',
    'param' => array('column' => 'name', 'value' => 'Drupal caching mode'),
    'value' => array('column' => 'value'),
    'limits' => array('warning' => 1, 'error' => 1),
  ));
  
  $requirements[] = backup_server_param_status($sid, array(
    'table' => 'backup_server_config',
    'param' => array('column' => 'name', 'value' => 'Drupal CSS compression'),
    'value' => array('column' => 'value'),
    'limits' => array('warning' => 1, 'error' => 1),
  ));
  
  $requirements[] = backup_server_param_status($sid, array(
    'table' => 'backup_server_config',
    'param' => array('column' => 'name', 'value' => 'Drupal JS compression'),
    'value' => array('column' => 'value'),
    'limits' => array('warning' => 1, 'error' => 1),
  ));
  
}

/**
 * Builds requirement array given set of parameters.
 *
 * @param $sid
 *   Site ID.
 * @param $params
 *   Drupal requirements array.
 * @returns
 *   (array) Requirements information
 */
function backup_server_param_status($sid, $params) {
  $value = backup_server_param_value($sid, $params);
  
  $output['title'] = ucfirst(str_replace('_', ' ', $params['param']['value']));
  if (isset($params['formatter'])) {
    if ($params['formatter'] == 'format_size') {
      $output['value'] = backup_server_format_size($value);
    }
  }
  else {
    $output['value'] = $value;
  }

  if ($params['order'] == 'less than') {
    if ($value <= $params['limits']['warning']) {
      $output['severity'] = 0;
    }
    elseif ($value < $params['limits']['error']) {
      $output['severity'] = 1;
    }
    else {
      $output['severity'] = 2;
    }
  }
  else {
    if ($value >= $params['limits']['warning']) {
      $output['severity'] = 0;
    }
    elseif ($value > $params['limits']['error']) {
      $output['severity'] = 1;
    }
    else {
      $output['severity'] = 2;
    }
  }
  $output['weight'] = 10;

  return $output;
}

/**
 * Returns value from database given a set of parameters.
 */
function backup_server_param_value($sid, $params) {
  $value = db_result(db_query("SELECT %s FROM {%s} WHERE %s = '%s' AND sid = %d", $params['value']['column'], $params['table'], $params['param']['column'], $params['param']['value'], $sid));
  
  return $value;
}

/**
 * Returns formatted number. Stolen from Drupal 7 format_size
 */
function backup_server_format_size($size, $langcode = NULL) {
  if ($size < 1024) {
    return format_plural($size, '1 byte', '@count bytes');
  }
  else {
    $size = $size / 1024; // Convert bytes to kilobytes.
    $units = array(
      t('@size KB'),
      t('@size MB'),
      t('@size GB'),
      t('@size TB'),
      t('@size PB'),
      t('@size EB'),
      t('@size ZB'),
      t('@size YB'),
      t('@size HB'), // Yes - 'Hella' is a unit.
    );
    foreach ($units as $unit) {
      if (round($size, 2) >= 1024) {
        $size = $size / 1024;
      }
      else {
        break;
      }
    }
    return str_replace('@size', round($size, 2), $unit);
  }
}

/* }@ */
