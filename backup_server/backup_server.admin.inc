<?php
/**
 * @file
 *   backup_server.admin.inc Provides admin forms for managing and
 *   configuring backup_server.
 *
 * @see backup_server.module
 *
 * @ingroup backup
 */

/** \addtogroup backup Backup Client */
/* @{ */


function backup_server_settings_form() {
  module_load_include('inc', 'backup_client', 'backup_client.helper');
  backup_client_check_advanced_help('backup_server', 'settings');

  for ($i = 0; $i <= 30; ++$i) {
    $options[$i] = $i;
  }

  $form['backup_server_options'] = array(
    '#type' => 'fieldset',
    '#title' => t('Overview limits'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    );

  $form['backup_server_options']['backup_server_tar_backup_warning'] = array(
    '#type' => 'select',
    '#title' => t('Backup warning threshold') .' (<img src="'. url('misc/watchdog-warning.png') . '" alt="warning icon" />)',
    '#default_value' => variable_get('backup_server_tar_backup_warning', '3'),
    '#description' => t('Limit in days.'),
    '#options' => $options,
  );

  $form['backup_server_options']['backup_server_tar_backup_error'] = array(
    '#type' => 'select',
    '#title' => t('Backup error threshold') .' (<img src="'. url('misc/watchdog-error.png') . '" alt="error icon" />)',
    '#default_value' => variable_get('backup_server_tar_backup_error', '7'),
    '#description' => t('Limit in days.'),
    '#options' => $options,
  );

  $form['backup_server_options']['backup_server_s3_backup_warning'] = array(
    '#type' => 'select',
    '#title' => t('S3 warning threshold') .' (<img src="'. url('misc/watchdog-warning.png') . '" alt="warning icon" />)',
    '#default_value' => variable_get('backup_server_s3_backup_warning', '3'),
    '#description' => t('Limit in days.'),
    '#options' => $options,
  );

  $form['backup_server_options']['backup_server_s3_backup_error'] = array(
    '#type' => 'select',
    '#title' => t('S3 error threshold') .' (<img src="'. url('misc/watchdog-error.png') . '" alt="error icon" />)',
    '#default_value' => variable_get('backup_server_s3_backup_error', '7'),
    '#description' => t('Limit in days.'),
    '#options' => $options,
  );

  return system_settings_form($form);
}

/**
 * Builds table of websites and appends form for updating information.
 *
 * @returns
 *  Backup admin page.
 */
function backup_server_sites_page() {
  module_load_include('inc', 'backup_server', 'backup_server.helper');
  module_load_include('inc', 'backup_client', 'backup_client.helper');
  backup_client_check_advanced_help('backup_server', 'overview');

  $pager_limit = variable_get('backup_client_pager_number', '10');

  $header = array(
    array('data' => t('Name') , 'field' => 'name', ),
    t('Backup<br />Warning > %warning days<br />Error > %error days', array('%warning' => variable_get('backup_server_tar_backup_warning', '3'), '%error' => variable_get('backup_server_tar_backup_error', '7'))),
    t('Amazon S3<br />Warning > %warning days<br />Error > %error days', array('%warning' => variable_get('backup_server_s3_backup_warning', '3'), '%error' => variable_get('backup_server_s3_backup_error', '7'))),
    t('Status'),
    t('Modules'),
    array('data' => t('Last update<br />(hours ago)'), 'field' => 'last'),
    t('Administration'),
  );

  $sql = "SELECT * FROM {backup_server_sites}";
  $result = pager_query($sql . tablesort_sql($header) , $pager_limit);
  while ($site = db_fetch_object($result)) {
    $rows[] = array(
      l($site->name, 'admin/build/backupserver/site/'. $site->sid),
      array(
        'data' => backup_server_status_backups($site->sid, 'tar_backup', variable_get('backup_server_tar_backup_warning', '3'), variable_get('backup_server_tar_backup_error', '7')),
        'align' => 'center',
      ),
      array(
        'data' => backup_server_status_backups($site->sid, 's3_backup', variable_get('backup_server_s3_backup_warning', '3'), variable_get('backup_server_s3_backup_error', '7')),
        'align' => 'center',
      ),
      array(
        'data' => backup_server_status_requirements($site->sid),
        'align' => 'center',
      ),
      array(
        'data' => backup_server_status_modules($site->sid),
        'align' => 'center',
      ),
      array(
        'data' => $site->last ? sprintf("%01.1f", round((time() - $site->last) / (60*60), 1)) : t('N/A'),
        'align' => 'center',
      ),
      array(
        'data' => 
          l(t('Update'), 'admin/build/backupserver/update/'. $site->sid) .' | '.
          l(t('Edit'), 'admin/build/backupserver/add/'. $site->sid) .' | '.
          l(t('Delete'), 'admin/build/backupserver/delete/'. $site->sid),
        'align' => 'center'
      ),
    );
  }

  if ($rows) {
    $pager = theme('pager', NULL, $pager_limit, 0);
    if (!empty($pager)) {
      $rows[] = array(array('data' => $pager, 'colspan' => count($rows[0])));
    }
    $output .= theme('table', $header, $rows);
  }
  else {
    drupal_set_message(t('Please add website.'));
    drupal_goto('admin/build/backupserver/add');
  }

  $output .= drupal_get_form('backup_server_update_all_form');

  return $output;

}

/**
 * Returns submit form.
 */
function backup_server_update_all_form() {

  $form['submit'] = array(
    '#type'           => 'submit',
    '#value'          => t('Update all'),
  );

  return $form;
}

/**
 * Submit handler to update all websites.
 */
function backup_server_update_all_form_submit($form, &$form_state) {
  module_load_include('inc', 'backup_client', 'backup_client.helper');
  module_load_include('inc', 'backup_server', 'backup_server.helper');

  // This can take a long time.
  backup_client_expand_environment();

  $result = db_query('SELECT sid FROM {backup_server_sites} ORDER BY name');

  while ($site = db_fetch_object($result)) {
    backup_server_update($site->sid, FALSE);
  }
}

/**
 * Display the form to create or edit website.
 */
function backup_server_add_edit_form() {
  module_load_include('inc', 'backup_client', 'backup_client.helper');
  backup_client_check_advanced_help('backup_server', 'add');

  $sid = arg(4);

  $website = db_fetch_object(db_query("SELECT name, url, username FROM {backup_server_sites} WHERE sid = '%s'", $sid));

  if ($website->name != '') {
    $form['sid'] = array(
      '#type' => 'hidden',
      '#value' => $sid,
    );

    foreach ($website as $key => $value) {
      $$key = $value;
    }
  }

  $form['fieldset'] = array(
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#tree' => TRUE,
  );

  $form['fieldset']['name'] = array(
    '#title'          => t('Site name'),
    '#type'           => 'textfield',
    '#default_value'  => isset($name) ? $name : '',
    '#description'    => t('Name or description of website.'),
    '#required'       => TRUE,
  );

  $form['fieldset']['url'] = array(
    '#title'          => t('Site URL'),
    '#type'           => 'textfield',
    '#default_value'  => isset($url) ? $url : '',
    '#description'    => t('The complete base URL of the website<br />(ie. http://example.com or http://example.com/drupal-subdomain).'),
    '#required'       => TRUE,
  );

  $form['fieldset']['username'] = array(
    '#title'          => t('Username'),
    '#type'           => 'textfield',
    '#default_value'  => isset($username) ? $username : '',
    '#description'    => t('Username that has permission to admin this site.'),
    '#required'       => TRUE,
  );

  $form['fieldset']['password'] = array(
    '#title'          => t('Username password'),
    '#type'           => 'password',
  );

  $form['submit'] = array(
    '#type'           => 'submit',
    '#value'          => t('Submit'),
  );

  return $form;
}

/**
 * Validation for backup_server_add_edit_form().
 */
function backup_server_add_edit_form_validate($form, &$form_state) {
  // If empty - don't erase prior values. Otherwise - else encode
  if (!isset($form_state['values']['sid'])) {
    $count = db_result(db_query("SELECT COUNT(sid) AS count FROM {backup_server_sites} WHERE url = '%s'", $form_state['values']['fieldset']['url']));

    if ($count > 0) {
      form_set_error('fieldset][url', t('You already have a website by this URL.'));
    }
  }
}

/**
 * Submit callback for services_keyauth_admin_keys_form().
 */
function backup_server_add_edit_form_submit($form, &$form_state) {
  $fields = $form_state['values']['fieldset'];
  $placeholders = db_placeholders($fields, 'varchar');

  // Do not change password if none given
  if ($fields['password'] == '' && isset($form_state['values']['sid'])) {
    $fields['password'] = db_result(db_query('SELECT password FROM {backup_server_sites} WHERE sid = %d', $form_state['values']['sid']));
  }
  // Otherwise encrypt
  else {
    $fields['password'] = md5($fields['password']);
  }

  if (isset($form_state['values']['sid'])) {
    $fields['sid'] = $form_state['values']['sid'];
    $placeholders .= ', %d';
  }

  db_query('REPLACE INTO {backup_server_sites} ('. implode(', ', array_keys($fields)) .') VALUES ('. $placeholders .')', $fields);

  $form_state['redirect']  =  'admin/build/backupserver';
}

/**
 * Show a form to confirm whether a website should be deleted.
 */
function backup_server_delete_confirm(&$form_state, $sid = 0) {
  $sid = arg(4);

  $name = db_result(db_query("SELECT name FROM {backup_server_sites} WHERE sid = %d", $sid));

  if ($name == '') {
    drupal_goto('admin/build/backupserver');
  }

  $form['sid'] = array('#type' => 'value', '#value' => $sid);
  $form['name'] = array(
    '#type' => 'hidden',
    '#value' => $name,
  );

  $message = t('Are you sure you want to delete the website %website?', array('%website' => $name));

  $caption = '<p>'. t('This action cannot be undone.') .'</p>';

  return confirm_form($form, $message, 'admin/build/backupserver', $caption, t('Delete'));
}

/**
 * Submit form callback for backup_server_delete_confirm().
 */
function backup_server_delete_confirm_submit($form, &$form_state) {

  $tables = array(
    'backup_server_backups',
    'backup_server_config',
    'backup_server_modules',
    'backup_server_mysql',
    'backup_server_settings',
    'backup_server_sites',
  );

  foreach ($tables as $table) {
    db_query('DELETE FROM {'. $table .'} WHERE sid = %d', $form_state['values']['sid']);
  }

  drupal_set_message(t('%website has been deleted.', array('%website' => $form_state['values']['name'])));

  $form_state['redirect'] = 'admin/build/backupserver';
}
/* }@ */

