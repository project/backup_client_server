<?php
/**
 * @file
 *  backup_client_s3.helper.inc Provides helper functions for executing
 *  Amazon S3 backups.
 *
 * @see backup_client_s3.module
 *
 * @ingroup backup
 */

/** \addtogroup backup Backup Client */
/* @{ */

/**
 * Class for Amazon S3 integration
 * http://code.google.com/p/amazon-s3-php-class/
 */
function backup_client_s3_object_check() {
  if (class_exists('S3')) {
    return TRUE;
  }

  if (file_exists(drupal_get_path('module', 'backup_client_S3') . '/../libraries/S3.php')) {

    require_once(drupal_get_path('module', 'backup_client_S3') . '/../libraries/S3.php');
    return TRUE;
  }

  watchdog('Backup Client S3', 'Could not find S3.php library', array(), WATCHDOG_ERROR);

  return FALSE;
}

/**
 * Return TRUE if connection can be make and bucket can be created.
 */
function backup_client_s3_test_config($access_key_id, $secret_access_key, $bucket) {
  // Create our Amazon S3 Service Interface
  $s3 = new S3($access_key_id, $secret_access_key, variable_get('backup_client_s3_use_SSL', 1));

  $bucket = backup_client_s3_generate_valid_bucket_name($bucket);

  // Check for and/or create bucket
  $buckets = $s3->listBuckets();

  if (!$buckets) {
    drupal_set_message(t('Cannot get list of buckets. Amazon S3 login may be incorrect. Please visit the !s3link', array('!s3link' => l(t('Amazon S3 configuration page'), 'admin/build/backupclient/settings/s3'))), 'error');
    watchdog('Backup Client S3', 'Cannot get list of buckets', array() , WATCHDOG_NOTICE);
    return FALSE;
  }

  if (!in_array($bucket, $buckets)) {
    // Create bucket
    if ( $s3->putBucket($bucket) ) {
      drupal_set_message(t('Bucket created: @bucket', array('@bucket' => $bucket)));
      watchdog('Backup Client S3', 'Bucket created: !bucket', array('!bucket' => $bucket) , WATCHDOG_NOTICE);
      return TRUE;

    }
    else {
      drupal_set_message(t('Could not make bucket: @bucket', array('@bucket' => $bucket)));
      watchdog('Backup Client S3', 'Could not make bucket: !bucket', array('!bucket' => $bucket) , WATCHDOG_NOTICE);
      return FALSE;
    }
  }
  else {
    return TRUE;
  }
}

/**
 * Returns bucket name either as default or processed string.
 */
function backup_client_s3_generate_valid_bucket_name($bucket = '') {

  global $base_url;

  if (!$bucket) {
    $bucket = variable_get('backup_client_s3_bucket', 'backupclients3-' . backup_client_site_url());
  }

  // Following S3 bucket naming guidelines
  // http://docs.amazonwebservices.com/AmazonS3/2006-03-01/index.html?UsingBucket.html
  $bucket = drupal_strtolower(drupal_substr(preg_replace('([^[:alnum:].])', '-', $bucket), 0, 63));
  return $bucket;
}

/**
 * Maintains sync between backup_client database table and Amazon S3 bucket.
 */
function backup_client_s3_sync_files() {

  if (!backup_client_s3_object_check()) {
    return;
  }

  $access_key_id = base64_decode(variable_get('backup_client_s3_access_key', ''));
  $secret_access_key = base64_decode(variable_get('backup_client_s3_secret_access_key', ''));


  if ($access_key_id == '' || $secret_access_key == '') {
    return;
  }

  // Create our Amazon S3 Service Interface
  $s3 = new S3($access_key_id, $secret_access_key, variable_get('backup_client_s3_use_SSL', 1));

  // Builds array of existing filenames.
  $bucket = backup_client_s3_generate_valid_bucket_name();
  $filelist = $s3->getBucket($bucket);
  $files = array();
  foreach ($filelist as $file) {
    $files[] =  $file['name'];
  }

  // Compares file status in database against existing files.
  $result = db_query('SELECT id, filename, status FROM {backup_client} WHERE type = "s3_backup"');
  while ($file = db_fetch_object($result)) {

    // Build array for reverse lookup next.
    $files_in_database[] = $file->filename;

    // Update status.
    if (!in_array($file->filename, $files)) {
      if ($file->status == TRUE) {
        db_query("UPDATE {backup_client} SET status = 0 WHERE id = %d", $file->id);
      }
    }
    else {
      if ($file->status == FALSE) {
        db_query("UPDATE {backup_client} SET status = 1 WHERE id = %d", $file->id);
      }
    }
  }

  // Check to see if there are files present that are not in database.
  foreach ((array) $files as $file) {
    if (!in_array($file, (array) $files_in_database) && !is_array($file)) {
      drupal_set_message(t('File found in S3 bucket that is not in backup client database table: @file', array('@file' => $file)), 'error');
    }
  }
}

/**
 * Returns array of files from Amazon S3 bucket.
 */
function backup_client_s3_get_files() {

  if (!class_exists('S3')) {
    return;
  }

  $access_key_id = base64_decode(variable_get('backup_client_s3_access_key', ''));
  $secret_access_key = base64_decode(variable_get('backup_client_s3_secret_access_key', ''));

  if ($access_key_id <> '' && $secret_access_key <> '') {
    // Create our Amazon S3 Service Interface
    $s3 = new S3($access_key_id, $secret_access_key, variable_get('backup_client_s3_use_SSL', 1));

    // Check for files
    $bucket = backup_client_s3_generate_valid_bucket_name();

    $files = $s3->getBucket($bucket);

    return $files;
  }
}

/**
 * Removes files from S3 bucket that are older than configuration setting.
 */
function backup_client_s3_delete() {
  $cutoff = strtotime(variable_get('backup_client_s3_age_to_keep', 'One year ago'));
  $files = backup_client_s3_get_files();

  foreach ($files as $file) {
    if ($file['time'] < $cutoff) {
      $files_to_remove[] = $file['name'];
    }
  }

  if ($files_to_remove) {
    $access_key_id = base64_decode(variable_get('backup_client_s3_access_key', ''));
    $secret_access_key = base64_decode(variable_get('backup_client_s3_secret_access_key', ''));

    // Create our Amazon S3 Service Interface
    $s3 = new S3($access_key_id, $secret_access_key, variable_get('backup_client_s3_use_SSL', 1));

    // Get/Generate bucket name
    $bucket = backup_client_s3_generate_valid_bucket_name();

    foreach ($files_to_remove as $file) {
      if ($s3->deleteObject($bucket, $file)) {
        ++$count;
      }
      else {
        watchdog('Backup Client S3', 'Could not remove: !file', array('!file' => $file) , WATCHDOG_WARNING);
      }
    }

    if ($count) {
      watchdog('Backup Client S3', '!count files removed', array('!count' => $count) , WATCHDOG_NOTICE);
    }
  }
}

/**
 * Uploads most recent tar to Amazon S3.
 */
function backup_client_s3_upload($note, $verbose = FALSE) {

  module_load_include('inc', 'backup_client', 'backup_client.helper');

  // Try to let the upload script run for 20 minutes.
  backup_client_change_env('max_execution_time', '1200', FALSE);

  // Get most recent tar backup
  $fileinfo = db_fetch_object(db_query_range("SELECT bid, filename, filepath FROM {backup_client} WHERE type = 'tar_backup' AND status = 1 ORDER BY bid DESC", 0, 1));

  $filename = $fileinfo->filename;
  $fullpath = $fileinfo->filepath;
  $bid = $fileinfo->bid;

  // Check for info
  if ($fullpath == '') {
    watchdog('Backup Client S3', 'No previous backup to upload', array() , WATCHDOG_WARNING);
    if ($verbose) {
      drupal_set_message(t('No previous backup to upload'), 'error');
    }
    return;
  }

  // Check for file
  if (!is_file($fullpath)) {
    watchdog('Backup Client S3', 'Could not locate: !fullpath', array('!fullpath' => $fullpath) , WATCHDOG_WARNING);
    if ($verbose) {
      drupal_set_message(t('Could not locate: @fullpath', array('@fullpath' => $fullpath)) , 'error');
    }
    return;
  }

  $access_key_id = base64_decode(variable_get('backup_client_s3_access_key', ''));
  $secret_access_key = base64_decode(variable_get('backup_client_s3_secret_access_key', ''));

  // Create our Amazon S3 Service Interface
  $s3 = new S3($access_key_id, $secret_access_key, variable_get('backup_client_s3_use_SSL', 1));

  // Get/Generate bucket name
  $bucket = backup_client_s3_generate_valid_bucket_name();

  // Check for and/or create bucket
  $buckets = $s3->listBuckets();
  if (!in_array($bucket, $buckets)) {

    // Create bucket
    if ($s3->putBucket($bucket)) {
      watchdog('Backup Client S3', 'Bucket created: !bucket', array('!bucket' => $bucket) , WATCHDOG_NOTICE);
      if ($verbose) {
        drupal_set_message(t('Bucket created: @bucket', array('@bucket' => $bucket)));
      }
    }
    else {
      watchdog('Backup Client S3', 'Could not make bucket: !bucket', array('!bucket' => $bucket) , WATCHDOG_WARNING);
      if ($verbose) {
        drupal_set_message(t('Could not make bucket: @bucket', array('@bucket' => $bucket)));
      }
      return;
    }
  }

  // Check for pre-existing file
  $ls_bucket = $s3->getBucket($bucket);
  if (array_key_exists($filename, $ls_bucket)) {
    watchdog('Backup Client S3', '!filename already exists. No S3 upload necessary.', array('!filename' => $filename) , WATCHDOG_NOTICE);
    if ($verbose) {
      drupal_set_message(t('@filename already exists in bucket. No S3 upload necessary.', array('@filename' => $filename)) , 'error');
    }
    return;
  }

  // Save file to S3
  $start = time();
  if ( $s3->putObject($s3->inputFile($fullpath, FALSE) , $bucket, $filename, S3::ACL_PRIVATE) ) {
    $end = time();
    $size  = filesize($fullpath);
    $formatsize = format_size($size);
    $diff = $end - $start;
    // 1 added to avoid division by zero
    $diff = $diff == 0 ? 1 : $diff;

    $rate = format_size($size / $diff);
    if ($rate > 0) {
      variable_set('backup_client_s3_rate', $rate);
    }

    $variables = array('@filename' => $filename, '@formatsize' => $formatsize, '@rate' => $rate, '@diff' => $diff);

    watchdog('Backup Client S3', '@filename uploaded to S3: @formatsize, @rate/sec, @diff sec.', $variables, WATCHDOG_NOTICE);
    if ($verbose) {
      drupal_set_message(t('@filename uploaded to S3: @formatsize, @rate/sec, @diff sec.', $variables));
    }

    // Write file data to database and dump before tar list is made
    db_query("INSERT INTO {backup_client} (bid, type, filename, filepath, note, status, started, duration, size) VALUES (%d, '%s', '%s', '%s', '%s', %d, %d, %d, %d)",
      $bid,
      's3_backup',
      $filename,
      $s3->getAuthenticatedURL($bucket, $filename, '600000'),
      $note,
      TRUE,
      $start,
      $diff,
      $size
    );

    // Used to trigger actions.
    module_invoke_all('backup_client_s3', 's3upload', $filename);

    return $report;

  }
  else {
    watchdog('Backup Client S3', 'Could Not Save File to S3.', array() , WATCHDOG_WARNING);
    if ($verbose) {
      drupal_set_message(t('Could Not Save File to S3.') , 'error');
    }
    return FALSE;
  }
}
