<?php
/**
 * @file
 *  backup_client_s3.admin.inc Provides admin forms for managing and
 *  configuring backup_client.
 *
 * @see backup_client_s3.module
 *
 * @ingroup backup
 */

/** \addtogroup backup Backup Client */
/* @{ */


/**
 * Returns admin page showing table of S3 uploads and upload form.
 */
function backup_client_s3_admin_page() {

  module_load_include('inc', 'backup_client', 'backup_client.helper');
  module_load_include('inc', 'backup_client_s3', 'backup_client_s3.helper');

  backup_client_check_advanced_help('backup_client_s3', 'introduction');
  backup_client_s3_object_check();

  if (!class_exists('S3')) {
    return '<h3>'. t('The S3.php cannot be found') . '</h3><p>'. t('Please download from !link and install in the <i>libraries</i> directory. <strong><br />NOTE: use version 0.3.9 *not* 0.4.0</strong>', array('!link' => '<a href="http://code.google.com/p/amazon-s3-php-class/downloads/list?can=1&q=&colspec=Filename+Summary+Uploaded+Size+DownloadCount">http://code.google.com</a>')) .'</p>';
  }

  if (variable_get('backup_client_s3_access_key', '') == '' || variable_get('backup_client_s3_secret_access_key', '') == '') {
    return '<h3>' . t('Amazon S3 access and secret keys have not been configured.') . '</h3><p>' . t('Please visit the !s3link', array('!s3link' => l(t('Amazon S3 configuration page'), 'admin/build/backupclient/configuration/s3'))) . '</p>';
    return;
  }

  backup_client_s3_sync_files();

  $pager_limit = variable_get('backup_client_pager_number', '10');

  $header = array(
    array('data' => t('#') , 'field' => 'bid', 'sort' => 'desc'),
    array('data' => t('Filename') , 'field' => 'filename', ),
    array('data' => t('Size') , 'field' => 'size', ),
    array('data' => t('Seconds'), 'field' => 'duration'),
    array('data' => t('Date') , 'field' => 'started', ),
    array('data' => t('Note') , 'field' => 'note', ),
  );

  $sql = "SELECT * FROM {backup_client} WHERE type = 's3_backup' AND status = 1";

  $result = pager_query($sql . tablesort_sql($header) , $pager_limit);
  while ($file = db_fetch_object($result)) {
    $rows[] = array(
      $file->bid,
      '<a href="'. check_plain($file->filepath) .'">'. check_plain($file->filename) .'</a>',
      format_size($file->size),
      $file->duration,
      date('Y-m-d h:iA', $file->started),
      check_plain($file->note),
    );
  }

  if ($rows) {
    $pager = theme('pager', NULL, $pager_limit, 0);
    if (!empty($pager)) {
      $rows[] = array(array('data' => $pager, 'colspan' => '5'));
    }
    $output .= theme('table', $header, $rows, array() , '<h3>'. t('Amazon S3 Uploads') .'</h3>');
  }
  else {
    $output .= t('No Amazon S3 uploads found.');
  }

  if (user_access('upload backups to s3')) {
    $output .= drupal_get_form('backup_client_s3_upload_now_form');
  }

  return $output;
}

/**
 *  Returns upload form.
 */
function backup_client_s3_upload_now_form() {

  $form['s3_note'] = array(
    '#type' => 'textfield',
    '#title' => t('Note for this S3 upload'),
  );

  $form['now'] = array(
    '#type' => 'submit',
    '#value' => t('Upload latest backup now'),
    );

  return $form;
}

/**
 * Submit handler for upload form.
 */
function backup_client_s3_upload_now_form_submit($form, &$form_state) {

  backup_client_s3_upload($form_state['values']['s3_note'], TRUE);
}

/**
 * Returns settings page.
 */
function backup_client_s3_settings_page() {

  module_load_include('inc', 'backup_client', 'backup_client.helper');
  module_load_include('inc', 'backup_client_s3', 'backup_client_s3.helper');

  backup_client_check_advanced_help('backup_client_s3', 'settings');

  backup_client_s3_object_check();
  if (!class_exists('S3')) {
    return '<h3>'. t('The S3.php cannot be found') . '</h3><p>'. t('Please download from !link and install in the <i><strong>libraries</strong></i> directory.', array('!link' => '<a href="http://code.google.com/p/amazon-s3-php-class">http://code.google.com/p/amazon-s3-php-class</a>')) .'</p>';
  }

  return drupal_get_form('backup_client_s3_settings_form');
}

/**
 * S3 options form.
 */
function backup_client_s3_settings_form() {

  module_load_include('inc', 'backup_client', 'backup_client.admin');

  drupal_add_js('
var map = {
  "#edit-backup-client-cron-s3-backup-on" : "#s3_options",
  "#edit-backup-client-s3-create-backups" : "#backup_client_s3_backup_frequency",
  "#edit-backup-client-cron-remove-s3-extra-backups" : "#edit-backup-client-s3-age-to-keep-wrapper"
};

$.each(map, function(checkbox, target) {
  $(checkbox).is(":checked") ? $(target).show() : $(target).hide();
  $(checkbox).change(function() {  $(target).toggle("slow"); } );
});', 'inline', 'footer');

  $configured = (variable_get('backup_client_s3_access_key', '') != '' && variable_get('backup_client_s3_secret_access_key', '') != '');

  $form['backup_client_s3_config'] = array(
    '#type' => 'fieldset',
    '#title' => $configured ? t('S3 Configuration (&#10004; complete)') : t('S3 configuration'),
    '#collapsible' => TRUE,
    '#collapsed' => $configured,
  );

  $form['backup_client_s3_config']['backup_client_s3_access_key'] = array(
    '#type' => 'password',
    '#title' => t('Access key'),
    '#description' => t('Password will not be shown after saving'),
  );

  $form['backup_client_s3_config']['backup_client_s3_secret_access_key'] = array(
    '#type' => 'password',
    '#title' => t('Secret access key'),
    '#description' => t('Password will not be shown after saving'),
  );

  $form['backup_client_s3_config']['backup_client_s3_bucket'] = array(
    '#type' => 'textfield',
    '#title' => t('Name of bucket (directory) to upload into.'),
    '#default_value' => backup_client_s3_generate_valid_bucket_name(),
    '#description' => t('Note: the name of the bucket needs to be unique across all of S3. If the bucket does not exist it will be created'),
  );

  $form['backup_client_s3_config']['backup_client_s3_use_SSL'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use SSL to communicate with Amazon S3.'),
    '#default_value' => variable_get('backup_client_s3_use_SSL', 1),
    '#description' => t('If your website is having difficulties connecting even though you have correctly entered your access and secret key you may need to disable SSL and/or correct your sever settings.'),
  );


  $form['backup_client_cron_s3_backup_on'] = array(
    '#type' => 'checkbox',
    '#title' => t('Allow cron to upload and/or remove Amazon S3 uploads.'),
    '#default_value' => variable_get('backup_client_cron_s3_backup_on', 0),
    '#description' => t('Each time Drupal cron is triggered - Backup Client S3 will check to see if a scheduled upload is needed. If so, the most recent backup will be uploaded to Amazon S3. If removal options are set - these will also be performed.'),
  );

  $form['backup_client_s3_options'] = array(
    '#type' => 'fieldset',
    '#title' => t('S3 upload options'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#prefix' => '<div id="s3_options">',
    );

  // Handle scheduled S3 backups
  $last_upload_time = db_result(db_query("SELECT MAX(started) FROM {backup_client} WHERE type = 's3_backup'"));

  if (backup_client_time_to_execute($last_upload_time, 'backup_client_s3_backup_frequency') &&
  variable_get('backup_client_s3_create_backups', 0) &&
  (variable_get('backup_client_s3_access_key', '') != '' && variable_get('backup_client_s3_secret_access_key', '') != '')) {
    drupal_set_message(t('Next cron will upload latest backup to Amazon S3'), 'status', FALSE);
  }

  $form['backup_client_s3_options']['backup_client_s3_create_backups'] = array(
  '#type' => 'checkbox',
  '#title' => t('Allow cron to upload backups to Amazon S3.'),
  '#default_value' => variable_get('backup_client_s3_create_backups', 0),
  );

  backup_client_date_form($form, 'backup_client_s3_options', 'backup_client_s3_backup_frequency');

  $form['backup_client_s3_removal_options'] = array(
    '#type' => 'fieldset',
    '#title' => t('S3 removal options'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    );

  $options = array(
      '1 second ago' => t('One second ago (for testing)'),
      '1 week ago' => t('One week ago'),
      '2 weeks ago' => t('Two weeks ago'),
      '3 weeks ago' => t('Three weeks ago'),
      '4 weeks ago' => t('Four weeks ago'),
      '2 months ago' => t('Two months ago'),
      '3 months ago' => t('Three months ago'),
      '6 months ago' => t('Six months ago'),
      '9 months ago' => t('Nine months ago'),
      '1 year ago' => t('One year ago'),
      );


  $form['backup_client_s3_removal_options']['backup_client_cron_remove_s3_extra_backups'] = array(
  '#type' => 'checkbox',
  '#title' => t('Allow cron to remove old S3 upload files.'),
  '#default_value' => variable_get('backup_client_cron_remove_s3_extra_backups', 0),
  );

  $form['backup_client_s3_removal_options']['backup_client_s3_age_to_keep'] = array(
    '#type' => 'select',
    '#title' => t('Remove Amazon S3 upload files from'),
    '#default_value' => variable_get('backup_client_s3_age_to_keep', '1 year ago'),
    '#options' => $options,
    '#prefix' => '<div style="margin-left: 2em;">',
    '#suffix' => '</div></div>',
  );

  return system_settings_form($form);
}

/**
 * Validation for settings. Handles empty fields and key validation.
 */
function backup_client_s3_settings_form_validate($form, &$form_state) {

  // If access keys are provided - check to see if they work
  if ($form_state['values']['backup_client_s3_access_key'] <> '' && $form_state['values']['backup_client_s3_secret_access_key'] <> '') {
    if (!backup_client_s3_test_config(
      $form_state['values']['backup_client_s3_access_key'],
      $form_state['values']['backup_client_s3_secret_access_key'],
      $form_state['values']['backup_client_s3_bucket'])
    ) {
      form_set_error('backup_client_s3_access_key', t('One of your configuration settings is not correct.'));
      form_set_error('backup_client_s3_secret_access_key', '');
    }
  }

  // If empty - don't erase prior values. Otherwise - else encode
  if ($form_state['values']['backup_client_s3_access_key'] == '') {
    $form_state['values']['backup_client_s3_access_key'] = variable_get('backup_client_s3_access_key', '');
  }
  else {
    $form_state['values']['backup_client_s3_access_key'] = base64_encode($form_state['values']['backup_client_s3_access_key']);
  }

  // If empty - don't erase prior values. Otherwise - else encode
  if ($form_state['values']['backup_client_s3_secret_access_key'] == '') {
    $form_state['values']['backup_client_s3_secret_access_key'] = variable_get('backup_client_s3_secret_access_key', '');
  }
  else {
    $form_state['values']['backup_client_s3_secret_access_key'] = base64_encode($form_state['values']['backup_client_s3_secret_access_key']);
  }
}
